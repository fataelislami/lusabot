var menuLeft = document.getElementById( 'cbp-spmenu-s2' ),
	body = document.body;

showMobileMenu.onclick = function() {
	classie.toggle( this, 'active' );
	classie.toggle( menuLeft, 'cbp-spmenu-open' );
	disableOther( 'showMobileMenu' );
};

// Close Right Side, click everywhere
$(document).mouseup(function (e) {
	var searchcontainer = $(".cbp-spmenu-open");
	if (!searchcontainer.is(e.target) // if the target of the click isn't the container...
	&& searchcontainer.has(e.target).length === 0) // ... nor a descendant of the container
	{
		classie.remove(menuLeft, 'cbp-spmenu-open');
		$('body').removeClass('menu-open-overlay__pink');
	}
});

// Close Button Right Side
function closeLeftSide() {
	classie.remove(menuLeft, 'cbp-spmenu-open');
	$('body').removeClass('menu-open-overlay__pink');
}

// Overlay Body Color
$(function() {
    $('#showMobileMenu').on('click', function() {
        $('body').toggleClass('menu-open-overlay__pink');
    });
});