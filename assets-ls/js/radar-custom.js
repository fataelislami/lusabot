let ctx = document.getElementById('PemetaanSkill').getContext('2d');
let chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'radar',

    // The data for our dataset
    data: {
        labels: ['Digital Marketing', 'Komunikasi', 'Seni', 'Personal Finance', 'Sport'],
        datasets: [{
            label: 'Pemetaan Skill',
            backgroundColor: '#00cc99',
            borderColor: '#00cc99',
            data: [40, 35, 45, 32, 27]
        }]
    },

    // Configuration options go here
    options: {
        tooltips: {
            callbacks: {
                label: function (tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }

                    label += Math.round(tooltipItem.yLabel * 100) / 100;
                    return label;
                }
            }
        },
        labels: {
            fontColor: '#424242',
            fontSize: 14
        }
    }
});