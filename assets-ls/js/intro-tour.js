// Class Intro Tour
function ClassIntro() {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                element: '#TourClass1',
                intro: "<div class='content-tour'><div class='content-title'>Informasi Kelas</div><div class='content-desc'>Kamu dapat melihat informasi tentang durasi kelas, jumlah modul, dll.</div></div>",
                position: 'top'
            },
            {
                element: '#TourClass2',
                intro: "<div class='content-tour'><div class='content-title'>Informasi Kelas</div><div class='content-desc'>Kamu dapat melihat informasi harga, rating, dan jumlah ulasan dari alumni kelas ini.</div></div>",
                position: 'right'
            }
        ]
    });
    intro.start();
}

// List Class Created Intro Tour
function ListClassCreatedIntro() {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                element: '#TourListClassCreated1',
                intro: "<div class='content-tour'><div class='content-title'>Info Cepat</div><div class='content-desc'>Berikut adalah ringkasan informasi tentang jumlah pendapatan, murid, dan kelas.</div></div>",
                position: 'bottom'
            }
        ]
    });
    intro.start();
}

// Detail Class Intro Tour
function DetailClassIntro() {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                element: '#TourDetailClass1',
                intro: "<div class='content-tour'><div class='content-title'>Kelebihan Kelas</div><div class='content-desc'>Kelebihan kelas yang bisa kamu nikmati yang terdapat di kelas lain</div></div>",
                position: 'top'
            },
            {
                element: '#TourDetailClass2',
                intro: "<div class='content-tour'><div class='content-title'>Deskripsi Lengkap Kelas</div><div class='content-desc'>Sebelum mendaftar pada kelas ini, harap kamu membaca deskripsinya terlebih dahulu.</div></div>",
                position: 'top'
            },
            {
                element: '#TourDetailClass3',
                intro: "<div class='content-tour'><div class='content-title'>Apa yang akan dipelajari</div><div class='content-desc'>Berikut adalah apa yang akan kamu pelajari ketika mendaftar pada kelas ini.</div></div>",
                position: 'top'
            },
            {
                element: '#TourDetailClass4',
                intro: "<div class='content-tour'><div class='content-title'>Modul Kelas</div><div class='content-desc'>Penjelasan singkat modul-modul kelas yang akan dipelajari, bisa dibaca disini.</div></div>",
                position: 'top'
            }
        ]
    });
    intro.start();
}

// Detail All Materi Intro Tour
function DetailAllMateriIntro() {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                element: '#TourDetailAllMateri1',
                intro: "<div class='content-tour'><div class='content-title'>Detail Materi</div><div class='content-desc'>Berikut adalah detail materi yang akan kamu pelajari dikelas ini.</div></div>",
                position: 'top'
            },
            {
                element: '#TourDetailAllMateri2',
                intro: "<div class='content-tour'><div class='content-title'>Sertifikat Peserta</div><div class='content-desc'>Kamu dapat mengunduh sertifikat peserta sebagai tanda telah mendaftar pada kelas ini.</div></div>",
                position: 'top'
            }
        ]
    });
    intro.start();
}

// Format Materi Intro Tour
function FormatMateriIntro() {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                element: '#TourFormatMateri1',
                intro: "<div class='content-tour'><div class='content-title'>Format Materi</div><div class='content-desc'>Kamu dapat menikmati materi kelas ini dengan berbagai format materi yang menarik.</div></div>",
                position: 'top'
            }
        ]
    });
    intro.start();
}

// Manage Keuangan Intro Tour
function ManageKeuanganIntro() {
    var intro = introJs();
    intro.setOptions({
        steps: [
            {
                element: '#TourManageKeuangan1',
                intro: "<div class='content-tour'><div class='content-title'>Penarikan Uang</div><div class='content-desc'>Kamu bisa melihat jumlah saldo yang ada dan bisa menarik uang tersebut kapan saja.</div></div>",
                position: 'bottom'
            },
            {
                element: '#TourManageKeuangan2',
                intro: "<div class='content-tour'><div class='content-title'>Informasi Rekening</div><div class='content-desc'>Berikut adalah informasi dari rekening yang kamu gunakan dan kamu bisa mengubahnya.</div></div>",
                position: 'bottom'
            }
        ]
    });
    intro.start();
}