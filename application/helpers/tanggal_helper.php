<?php
function formatHariTanggal($waktu)
	{
		$hari_array = [
			'Minggu',
			'Senin',
			'Selasa',
			'Rabu',
			'Kamis',
			'Jumat',
			'Sabtu'
		];

		$hr = date('w', strtotime($waktu));
		$hari = $hari_array[$hr];

		$tanggal = date('j', strtotime($waktu));

		$bulan_array = [
			1 => 'Januari',
			2 => 'February',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'November',
			12 => 'Desember',
		];

		$bl = date('n', strtotime($waktu));
		$bulan = $bulan_array[$bl];
		$tahun = date('Y', strtotime($waktu));

		return "$tanggal-$bulan-$tahun";
	}

	function formatHariTanggal2($waktu)
		{
			$hari_array = [
				'Minggu',
				'Senin',
				'Selasa',
				'Rabu',
				'Kamis',
				'Jumat',
				'Sabtu'
			];

			$hr = date('w', strtotime($waktu));
			$hari = $hari_array[$hr];

			$tanggal = date('j', strtotime($waktu));
			$jam=explode(" ",$waktu);
			$jam=explode(":",$jam[1]);
			$jam=$jam[0].":".$jam[1];
			$bulan_array = [
				1 => 'Januari',
				2 => 'February',
				3 => 'Maret',
				4 => 'April',
				5 => 'Mei',
				6 => 'Juni',
				7 => 'Juli',
				8 => 'Agustus',
				9 => 'September',
				10 => 'Oktober',
				11 => 'November',
				12 => 'Desember',
			];

			$bl = date('n', strtotime($waktu));
			$bulan = $bulan_array[$bl];
			$tahun = date('Y', strtotime($waktu));

			return "$hari, $tanggal $bulan $tahun | Pukul $jam";
		}//oke

		function tanggal_tiket_offline($waktu,$type)
			{
				$hari_array = [
					'Minggu',
					'Senin',
					'Selasa',
					'Rabu',
					'Kamis',
					'Jumat',
					'Sabtu'
				];

				$hr = date('w', strtotime($waktu));
				$hari = $hari_array[$hr];

				$tanggal = date('j', strtotime($waktu));

				$bulan_array = [
					1 => 'Januari',
					2 => 'February',
					3 => 'Maret',
					4 => 'April',
					5 => 'Mei',
					6 => 'Juni',
					7 => 'Juli',
					8 => 'Agustus',
					9 => 'September',
					10 => 'Oktober',
					11 => 'November',
					12 => 'Desember',
				];

				$bl = date('n', strtotime($waktu));
				$bulan = $bulan_array[$bl];
				$tahun = date('Y', strtotime($waktu));
				if($type=='hari'){
					return "$hari";
				}else if($type=='tanggal'){
					return "$tanggal $bulan $tahun";
				}
			}

 ?>
