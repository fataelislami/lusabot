<?php
class Course_model extends CI_Model{
    function getAllData(){
        $this->db->select('course.course_id,course.subject,course.briefing,course.duration,course.price,course.discount,course.price_cut,course.image,course.level,course.slug,course.type,course.created_on');
        $this->db->select('users.fullname,users.photo as users_photo');
        $this->db->select('count(chapter.chapter_id) as jumlah_modul');
        $this->db->select('table_ulasan.jumlah_ulasan');
        $this->db->select('table_ulasan.rata_rata');
            $this->db->from('category cat');
            $this->db->join('course', 'course.category_id = cat.category_id', 'inner');
            $this->db->join('users', 'users.users_id = course.user_id', 'inner');
        $this->db->join('chapter', 'chapter.course_id = course.course_id', 'left');
        //Join table denga subquery untuk mendapat nilai ULASAN
        $this->db->join('
                        (
                          SELECT course_user.course_id,count(course_user.course_user_id) AS jumlah_ulasan,(sum(course_user.rating)/count(course_user.course_user_id)) AS rata_rata
                          FROM `course_user`
                          LEFT JOIN course ON course_user.course_id=course.course_id
                          WHERE course_user.rating<>0 AND course.status=2
                          GROUP BY course_user.course_id
                        ) 
                        AS table_ulasan
                        ', 
                        'table_ulasan.course_id = course.course_id', 'left'
                      );
        $this->db->where('course.status', 2);
        $this->db->order_by('course.created_on','DESC');
        $this->db->group_by('chapter.course_id');
        $this->db->limit(9, 0);
        $query = $this->db->get();
        return $query;  
    }
    function getRelatedCourse($gender,$job){
      $this->db->select("course.course_id,course.subject,AVG(course_user.rating) as rating");
      $this->db->from("course");
      $this->db->join("course_user","course_user.course_id=course.course_id");
      $this->db->join("users","course_user.user_id=users.users_id");
      $this->db->where("users.gender='$gender' and users.job='$job' and rating IS NOT NULL");
      $this->db->group_by("course_user.course_id");
      $this->db->order_by("rating","DESC");
      $this->db->limit(9);
      return $this->db->get();
    }

    function getCoursebyId($course_id){
      $this->db->select('course.course_id,course.subject,course.briefing,course.duration,course.description,course.price,course.discount,course.price_cut,course.image,course.level,course.slug,course.type');
      $this->db->select('users.fullname,users.photo as users_photo');
      $this->db->select('cat.title as category');
      $this->db->select('count(chapter.chapter_id) as jumlah_modul');
      $this->db->select('table_ulasan.jumlah_ulasan');
      $this->db->select('table_ulasan.rata_rata');
          $this->db->from('category cat');
          $this->db->join('course', 'course.category_id = cat.category_id', 'inner');
          $this->db->join('users', 'users.users_id = course.user_id', 'inner');
      $this->db->join('chapter', 'chapter.course_id = course.course_id', 'left');
      //Join table denga subquery untuk mendapat nilai ULASAN
      $this->db->join('
                      (
                        SELECT course_user.course_id,count(course_user.course_user_id) AS jumlah_ulasan,(sum(course_user.rating)/count(course_user.course_user_id)) AS rata_rata
                        FROM `course_user`
                        LEFT JOIN course ON course_user.course_id=course.course_id
                        WHERE course_user.rating<>0 AND course.status=2
                        GROUP BY course_user.course_id
                      ) 
                      AS table_ulasan
                      ', 
                      'table_ulasan.course_id = course.course_id', 'left'
                    );
      $this->db->where('course.status', 2);
      $this->db->where('course.course_id',$course_id);
      $this->db->group_by('chapter.course_id');
      $this->db->limit(9, 0);
      $query = $this->db->get();
      return $query;  
  }

  function chapter($course_id){
    $this->db->select('chapter.chapter_id,chapter.title as chapter_title,chapter.briefing as chapter_briefing,chapter.is_subchapter');
    $this->db->select('GROUP_CONCAT(subchapter.title ORDER BY subchapter.subchapter_id ASC SEPARATOR "#") as subchapter_title');
    $this->db->select('GROUP_CONCAT(subchapter.subchapter_id ORDER BY subchapter.subchapter_id ASC SEPARATOR "#") as subchapter_id');
    // $this->db->select('subchapter.video as video,subchapter.audio as audio,subchapter.ebook as ebook');
    $this->db->select('course.slug as slug,course.subject as subject,course.image as image');
    $this->db->from('chapter');
    $this->db->join('subchapter', 'subchapter.chapter_id = chapter.chapter_id', 'left');
    $this->db->join('course', 'course.course_id = chapter.course_id', 'left');
    $this->db->group_by('subchapter.chapter_id');
    $this->db->where('course.course_id', $course_id);
    $this->db->where('course.status', 2);
    return $this->db->get();
  }
  function subchapter($chapter_id){
    $this->db->select('chapter.chapter_id,chapter.title as chapter_title,chapter.is_subchapter');
    $this->db->select("course.subject as subject");
    $this->db->select('subchapter.subchapter_id,subchapter.title,subchapter.video as video,subchapter.audio as audio,subchapter.ebook as ebook');
    $this->db->from('chapter');
    $this->db->join('subchapter', 'subchapter.chapter_id = chapter.chapter_id', 'left');
    $this->db->join('course', 'course.course_id = chapter.course_id', 'left');
    $this->db->group_by('subchapter.chapter_id');
    $this->db->where('chapter.chapter_id', $chapter_id);
    $this->db->where('course.status', 2);
    return $this->db->get();
  }
}