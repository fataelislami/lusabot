<?php
class Users_model extends CI_Model{
    function getCourse($limit=9,$offset=0,$user_id=null,$finish=0)
  {
    $this->db->select('course.course_id,course.subject,course.briefing,course.duration,course.price,course.discount,course.price_cut,course.image,course.level,course.slug,course.type');
    $this->db->select('course_user.course_user_id');
    $this->db->select('cat.title as category');
    $this->db->select('users.fullname,users.photo as users_photo');
    $this->db->select('table_ulasan.jumlah_ulasan');
    $this->db->select('table_ulasan.rata_rata');
    $this->db->from('category cat');
    $this->db->join('course', 'course.category_id = cat.category_id', 'inner');
    $this->db->join('users', 'users.users_id = course.user_id', 'inner');
    $this->db->join('course_user','course_user.course_id=course.course_id');
    $this->db->join('payments','payments.payments_id=course_user.payments_id');
    //Join table dengan subquery untuk mendapat nilai ULASAN
    $this->db->join('
                      (
                        SELECT course_user.course_id,count(course_user.course_user_id) AS jumlah_ulasan,(sum(course_user.rating)/count(course_user.course_user_id)) AS rata_rata
                        FROM `course_user`
                        LEFT JOIN course ON course_user.course_id=course.course_id
                        WHERE course_user.rating<>0 AND course.status=2
                        GROUP BY course_user.course_id
                      ) 
                      AS table_ulasan
                      ', 
                      'table_ulasan.course_id = course.course_id', 'left'
                    );
    $this->db->where('course.status', 2);
    if($user_id!=null){
      $this->db->where('course_user.user_id', $user_id);
    }
    if($finish==0){
      $this->db->where("(DATEDIFF(NOW(), payments.pay_date) <=course_user.duration)");
    }else{
      $this->db->where("(DATEDIFF(NOW(), payments.pay_date) >=course_user.duration)");
    }
    $this->db->where("payments.status", 1);
    $this->db->limit($limit,$offset);
    $this->db->order_by('payments.pay_date',"DESC");
    $query = $this->db->get();
    return $query;
  }

  function course_user($course_user_id){
    $this->db->select("*");
    $this->db->from("course_user");
    $this->db->where("course_user_id",$course_user_id);
    return $this->db->get();
  }
  function update($where,$data,$to){
    $this->db->where($where);
    $db=$this->db->update($to,$data);
    if ($this->db->affected_rows()>0) {
      return true;
      }else{
      return false;
      }
  }
  function getDataCertificate($course_user_id){
    $this->db->select('exam_user.exam_user_id as id,MAX(exam_user.score) as score,exam_user.taken_date,course.subject,course.signature,users.fullname as creator,course_user.user_id as member_id,mt.fullname');
    $this->db->from('exam_user');
    $this->db->join('course_user', 'course_user.course_user_id = exam_user.course_user_id');
    $this->db->join('course', 'course.course_id = course_user.course_id');
    $this->db->join('users', 'users.users_id = course.user_id');
    $this->db->join('(SELECT users.users_id,users.fullname FROM users) as mt','mt.users_id=course_user.user_id');
    $this->db->where('course_user.course_user_id', $course_user_id);
    return $this->db->get();
  }
}