<?php
class Transaction_model extends CI_Model{
    function insert($data,$to){
        $insert = $this->db->insert($to, $data);
        if ($this->db->affected_rows()>0) {
          return true;
          }else{
          return false;
          }
    }
    function update($where,$data,$to){
        $this->db->where($where);
        $db=$this->db->update($to,$data);
        if ($this->db->affected_rows()>0) {
          return true;
          }else{
          return false;
          }
    }
    function delete($from,$where){
        $this->db->where($where);
        $this->db->delete($from);
    }
    function getLineIdentity($order_id){
        $this->db->select('users.line_identity');
        $this->db->from('users');
        $this->db->join('course_user','course_user.user_id=users.users_id');
        $this->db->join('payments','payments.payments_id=course_user.payments_id');
        $this->db->where('payments.midtrans_id',$order_id);
        return $this->db->get();
    }

    function kelas_user($course_id,$user_id){
        $this->db->select('course_user.*');
        $this->db->select('payments.pay_date,payments.status as payment_status');
        $this->db->from('course_user');
        $this->db->join('payments', 'payments.payments_id = course_user.payments_id');
        $this->db->join('course', 'course.course_id = course_user.course_id', 'left');
        $this->db->join('category','category.category_id=course.category_id');
        $this->db->where('course.course_id', $course_id);
        $this->db->where('course_user.user_id',$user_id);
        $this->db->where("(DATEDIFF(NOW(), payments.pay_date) <=course_user.duration)");
        return $this->db->get();
    }
    function historyData($user_id){
        $this->db->select("course.subject,category.title as category,payments.pay_date,payments.snap_token,payments.status,course_user.price,course_user.course_id,course_user.duration,vouchers.code");
        $this->db->from('payments');
        $this->db->join('course_user','course_user.payments_id=payments.payments_id');
        $this->db->join('vouchers','vouchers.vouchers_id=payments.voucher_id','LEFT');
        $this->db->join('course','course.course_id=course_user.course_id');
        $this->db->join('category','category.category_id=course.category_id');
        $this->db->where('course_user.user_id',$user_id);
        $this->db->order_by('payments.pay_date','DESC');
        $this->db->limit(9);
        return $this->db->get();
    }
}