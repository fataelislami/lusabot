<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exam_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function insert($data,$to){
    $insert = $this->db->insert($to, $data);
    if ($this->db->affected_rows()>0) {
      return true;
      }else{
      return false;
      }
}
function update($where,$data,$to){
    $this->db->where($where);
    $db=$this->db->update($to,$data);
    if ($this->db->affected_rows()>0) {
      return true;
      }else{
      return false;
      }
}
function delete($from,$where){
    $this->db->where($where);
    $this->db->delete($from);
}

  function getCourseId($course_user_id){
    $this->db->select('course_id');
    $this->db->where('course_user_id',$course_user_id);
    return $this->db->get('course_user')->row()->course_id;
  }
  function getExamUser($course_user_id){
    $this->db->select("*");
    $this->db->from("exam_user");
    $this->db->where('course_user_id',$course_user_id);
    return $this->db->get();
  }

  function getMultiple($course_id){
    $this->db->select('exam.grade,exam.duration');
    $this->db->select('exam_quesan.*');
    $this->db->from('exam');
    $this->db->join('course', 'exam.course_id = course.course_id');
    $this->db->join('exam_quesan', 'exam_quesan.exam_id = exam.exam_id');
    $this->db->where('course.course_id', $course_id);
    return $this->db->get();
  }


}
