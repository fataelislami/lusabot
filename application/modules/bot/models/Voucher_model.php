<?php
class Voucher_model extends CI_Model{
    function checkData($where){
        $this->db->select("course.subject,category.title as category,course.price,course.duration,vouchers.code,vouchers.is_used,vouchers.course_id,vouchers.vouchers_id");
        $this->db->from("vouchers");
        $this->db->join('course','course.course_id=vouchers.course_id');
        $this->db->join('category','category.category_id=course.category_id');
        $this->db->where($where);
        return $this->db->get();
    }

    function update($where,$data,$to){
        $this->db->where($where);
        $db=$this->db->update($to,$data);
        if ($this->db->affected_rows()>0) {
          return true;
          }else{
          return false;
          }
    }
}