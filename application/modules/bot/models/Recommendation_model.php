<?php

class Recommendation_model extends CI_Model{
    function getRatingCourseUser(){
        $this->db->simple_query('SET SESSION group_concat_max_len=15000');
        $this->db->select("users.users_id,GROUP_CONCAT(course.subject) as subject,GROUP_CONCAT(DISTINCT course.course_id) as course_id,GROUP_CONCAT(course_user.rating) ratings,count(course_user.course_user_id) as jumlah_beli");
        $this->db->from("course_user");
        $this->db->join("users","users.users_id=course_user.user_id");
        $this->db->join("course","course.course_id=course_user.course_id");
        $this->db->join("payments","payments.payments_id=course_user.payments_id");
        $this->db->where("payments.status",1);
        $this->db->where("course_user.rating!=",0);
        $this->db->group_by("course_user.user_id");
        $this->db->order_by("jumlah_beli","DESC");
        return $this->db->get();
    }
}