<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="Demo project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Daftar - Luar Sekolah</title>
    <link rel="shortcut icon" type="image/png" href="<?= base_url()?>assets-ls/images/icons/favicon.png" />
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>vendor/font-awesome/css/font-awesome.min.css">
    <!-- Slick -->
    <link id="effect" rel="stylesheet" type="text/css" media="all" href="<?= base_url()?>vendor/slick/css/slick.css" />
    <link rel="stylesheet" type="text/css" media="all" href="<?= base_url()?>vendor/slick/css/slick-theme.css" />
    <!-- Hamburgers -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>vendor/hamburgers/css/hamburgers.min.css">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets-ls/css/main.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets-ls/css/animate.css">
</head>

<body>
    <div class="navbar navbar-expand-lg desktop-navigation other-navbar fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">
                    <img src="<?= base_url()?>assets-ls/images/icons/logo.png" alt="" class="img-fluid logo-home">
                    <img src="<?= base_url()?>assets-ls/images/icons/logo-merah.png" alt="" class="img-fluid logo-other">
                </a>
            </div>
        </div>
    </div>

    <div class="content-box content-other">
        <section class="content-blank">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-8 offset-md-2">
                        <h3 class="text-capitalize head-title text-center">Daftar akun</h3>
                        <p class="head-sub-desc text-center">Silakan lakukan pendaftaran untuk menggunakan<br>LINE Bot Luarsekolah</p>

                        
                        <br>
                        <form action="<?= base_url()?>bot/register/action" method="POST">
                        <div class="form-group mb-4">
                            <label for="" class="text-default font-600 mb-3">Nama Lengkap</label>
                            <input type="text" name="fullname" class="form-control" placeholder=""
                                aria-describedby="helpId">
                        </div>
                        <div class="form-group mb-4">
                            <label for="" class="text-default font-600 mb-3">Tanggal Lahir</label>
                            <input type="date" name="birthdate" class="form-control" placeholder=""
                                aria-describedby="helpId">
                        </div>
                        <div class="form-group mb-4">
                            <label for="" class="text-default font-600 mb-3">Jenis Kelamin</label>
                            <select name="gender" class="form-control">
                                <option value="">Pilih</option>
                                <option value="male">Laki-laki</option>
                                <option value="female">Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group mb-4">
                            <label for="" class="text-default font-600 mb-3">Pekerjaan</label>
                            <div [formgroup]="radioGroup">
                                <label class="radio radio-custom pl-5 mb-3">
                                    <input type="radio" name="job" value="pengusaha"  formcontrolname="radio">
                                    <div class="card rad-5">
                                        <div class="card-body p-t-12 p-b-12">
                                            <p class="mb-0">Pengusaha</p>
                                        </div>
                                    </div>
                                    <span class="checkmark mt-2"></span>
                                </label>

                                <label class="radio radio-custom pl-5 mb-3">
                                <input type="radio" name="job" value="karyawan" formcontrolname="radio">
                                    <div class="card rad-5">
                                        <div class="card-body p-t-12 p-b-12">
                                            <p class="mb-0">Karyawan</p>
                                        </div>
                                    </div>
                                    <span class="checkmark mt-2"></span>
                                </label>

                                <label class="radio radio-custom pl-5 mb-3">
                                <input type="radio" name="job" value="freelancer" formcontrolname="radio">
                                    <div class="card rad-5">
                                        <div class="card-body p-t-12 p-b-12">
                                            <p class="mb-0">Freelancer</p>
                                        </div>
                                    </div>
                                    <span class="checkmark mt-2"></span>
                                </label>

                                <label class="radio radio-custom pl-5 mb-3">
                                <input type="radio" name="job" value="mahasiswa" formcontrolname="radio">
                                    <div class="card rad-5">
                                        <div class="card-body p-t-12 p-b-12">
                                            <p class="mb-0">Mahasiswa</p>
                                        </div>
                                    </div>
                                    <span class="checkmark mt-2"></span>
                                </label>
                                <label class="radio radio-custom pl-5 mb-3">
                                <input type="radio" name="job" value="siswa" formcontrolname="radio">
                                    <div class="card rad-5">
                                        <div class="card-body p-t-12 p-b-12">
                                            <p class="mb-0">Siswa</p>
                                        </div>
                                    </div>
                                    <span class="checkmark mt-2"></span>
                                </label>

                            </div>
                        </div>
                        <div class="form-group mb-4">
                            <label for="" class="text-default font-600 mb-3">E-mail</label>
                            <input type="email" name="email" class="form-control" placeholder=""
                                aria-describedby="helpId">
                        </div>
                    
                        <div class="form-group mb-4">
                            <label for="" class="text-default font-600 mb-3 w-100">
                                Kata Sandi
                            </label>
                            <input type="password" name="password" id="" class="form-control" placeholder=""
                                aria-describedby="helpId">
                        </div>
                        
                        <input type="hidden" name="line_identity" value="<?= $line_identity?>">
                        <br>
                        <button tyle="submit" class="btn btn-danger w-100 btn-lg">Daftar</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        
    </div>

    <script type="text/javascript" src="<?= base_url()?>vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/popper/popper.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/slick/js/slick.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/animsition/js/animsition.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets-ls/js/main.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets-ls/js/slick-add.js"></script>

</body>

</html>