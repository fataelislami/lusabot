<?php
    if($this->session->flashdata('message')) {
        $flashMessage=$this->session->flashdata('message');
        echo "<script>alert('$flashMessage')</script>";
    }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="Demo project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ujian - Luar Sekolah</title>
    <link rel="shortcut icon" type="image/png" href="<?= base_url()?>assets-ls/images/icons/favicon.png" />
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>vendor/font-awesome/css/font-awesome.min.css">
    <!-- Slick -->
    <link id="effect" rel="stylesheet" type="text/css" media="all" href="<?= base_url()?>vendor/slick/css/slick.css" />
    <link rel="stylesheet" type="text/css" media="all" href="<?= base_url()?>vendor/slick/css/slick-theme.css" />
    <!-- Hamburgers -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>vendor/hamburgers/css/hamburgers.min.css">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets-ls/css/main.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets-ls/css/animate.css">
</head>

<body>
    <div class="navbar navbar-expand-lg desktop-navigation other-navbar fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">
                    <img src="<?= base_url()?>assets-ls/images/icons/logo.png" alt="" class="img-fluid logo-home">
                    <img src="<?= base_url()?>assets-ls/images/icons/logo-merah.png" alt="" class="img-fluid logo-other">
                </a>
            </div>
        </div>
    </div>

    <div class="content-box content-other">
    <section class="content-blank pt-3">
    <div class="container mb-4">
        <nav class="mb-2">
            <div class="nav nav-tabs nav-fill">
                <a class="nav-item active" href="#">Ujian Kelas</a>
            </div>
        </nav>
        <?php if($loadExam->num_rows()>0){ ?>
        <div class="row mt-4">
            <div class="col-12 col-md-8 offset-md-2">
                <h3 class="text-capitalize head-title text-center">Ujian Kelas</h3>
                <p class="head-sub-desc text-center">
                    Selamat Mengerjakan Soal Ujian!

                    <!--   Kamu dapat mengakses kelas ini sampai tanggal <span class="text-danger font-400">30 Mei 2019 Pukul 23.59 WIB.</span> -->
                </p>
            </div>
        </div>
        <form action="<?= base_url() ?>ujian/multiple_action" method="post">
        <div class="row mt-3">
            <div class="col-12 col-md-8 offset-md-2">
              <?php $nomor=1; ?>
              <?php foreach ($loadExam->result() as $key): ?>
                <div class="mb-4">
                    <p class="text-default font-500 font-16 ls-93">
                        <?= $nomor ?>.) <?= $key->question ?>
                    </p>
                    <input type="hidden" name="jawaban[]" value="<?= $key->answer ?>">
                    <fieldset id="number<?= $nomor ?>" [formgroup]="radioGroup">
                        <label class="radio radio-check">
                            <input type="radio" name="pilihan<?= $nomor ?>" value="1" formcontrolname="radio" required>
                            <span class="title">
                                <div class="card rad-5 mb-3">
                                    <div class="content p-3">
                                        <span class="font-desc font-500 font-16 ls-93 mb-0">A. <?= $key->choice1 ?></span>
                                    </div>
                                </div>
                            </span>
                            <span class="checkmark"></span>
                        </label>

                        <label class="radio radio-check">
                            <input type="radio" name="pilihan<?= $nomor ?>" value="2" formcontrolname="radio" required>
                            <span class="title">
                                <div class="card rad-5 mb-3">
                                    <div class="content p-3">
                                        <span class="font-desc font-500 font-16 ls-93 mb-0">B. <?= $key->choice2 ?></span>
                                    </div>
                                </div>
                            </span>
                            <span class="checkmark"></span>
                        </label>

                        <label class="radio radio-check">
                            <input type="radio" name="pilihan<?= $nomor ?>" value="3" formcontrolname="radio" required>
                            <span class="title">
                                <div class="card rad-5 mb-3">
                                    <div class="content p-3">
                                        <span class="font-desc font-500 font-16 ls-93 mb-0">C. <?= $key->choice3 ?></span>
                                    </div>
                                </div>
                            </span>
                            <span class="checkmark"></span>
                        </label>

                        <label class="radio radio-check">
                            <input type="radio" name="pilihan<?= $nomor ?>" value="4" formcontrolname="radio" required>
                            <span class="title">
                                <div class="card rad-5 mb-3">
                                    <div class="content p-3">
                                        <span class="font-desc font-500 font-16 ls-93 mb-0">D. <?= $key->choice4 ?></span>
                                    </div>
                                </div>
                            </span>
                            <span class="checkmark"></span>
                        </label>
                    </fieldset>
                </div>
                <?php $nomor++ ?>
              <?php endforeach; ?>
              <input type="hidden" name="exam_user_id" value="<?= $exam_user_id ?>">
              <input type="hidden" name="course_id" value="<?= $course_id ?>">
              <input type="hidden" name="course_user_id" value="<?= $course_user_id ?>">
              <input type="hidden" name="totalNomor" value="<?= $nomor ?>">
                <br>
                <input type="submit" class="btn btn-primary w-100" name="submit" value="Submit" onsubmit="return confirm('Anda yakin untuk selesai?');">
            </div>
        </div>
        </form>
      <?php }else{ ?>
        <div class="row mt-4">
            <div class="col-12 col-md-8 offset-md-2">
                <h3 class="text-capitalize head-title text-center">Ujian Kelas</h3>
                <p class="head-sub-desc text-center">
                    Belum ada soal ujian yang ditambahkan, silakan hubungi administrator

                    <!--   Kamu dapat mengakses kelas ini sampai tanggal <span class="text-danger font-400">30 Mei 2019 Pukul 23.59 WIB.</span> -->
                </p>
            </div>
        </div>
      <?php } ?>
    </div>
</div>
</section>

        
    </div>

    <script type="text/javascript" src="<?= base_url()?>vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/popper/popper.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/slick/js/slick.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/animsition/js/animsition.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets-ls/js/main.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets-ls/js/slick-add.js"></script>

</body>

</html>



