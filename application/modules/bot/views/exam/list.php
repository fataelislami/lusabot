<?php
    if($this->session->flashdata('message')) {
        $flashMessage=$this->session->flashdata('message');
        echo "<script>alert('$flashMessage')</script>";
    }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="Demo project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ujian - Luar Sekolah</title>
    <link rel="shortcut icon" type="image/png" href="<?= base_url()?>assets-ls/images/icons/favicon.png" />
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>vendor/font-awesome/css/font-awesome.min.css">
    <!-- Slick -->
    <link id="effect" rel="stylesheet" type="text/css" media="all" href="<?= base_url()?>vendor/slick/css/slick.css" />
    <link rel="stylesheet" type="text/css" media="all" href="<?= base_url()?>vendor/slick/css/slick-theme.css" />
    <!-- Hamburgers -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>vendor/hamburgers/css/hamburgers.min.css">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets-ls/css/main.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets-ls/css/animate.css">
</head>

<body>
    <div class="navbar navbar-expand-lg desktop-navigation other-navbar fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">
                    <img src="<?= base_url()?>assets-ls/images/icons/logo.png" alt="" class="img-fluid logo-home">
                    <img src="<?= base_url()?>assets-ls/images/icons/logo-merah.png" alt="" class="img-fluid logo-other">
                </a>
            </div>
        </div>
    </div>

    <div class="content-box content-other">
    <section class="content-blank pt-3">
    <div class="container mb-4">

        <nav class="mb-2">
            <div class="nav nav-tabs nav-fill">
                <a class="nav-item active" href="#">Ujian Kelas</a>
            </div>
        </nav>

        <?php if($loadExam->num_rows()>0){ ?>
        <div class="row mt-4">
            <div class="col-12 col-md-8 offset-md-2">
                <h3 class="text-capitalize head-title text-center">Ujian Kelas</h3>
                <p class="head-sub-desc text-center">
                    Data Hasil Ujian Kelas
                    <!--   Kamu dapat mengakses kelas ini sampai tanggal <span class="text-danger font-400">30 Mei 2019 Pukul 23.59 WIB.</span> -->
                </p>
            </div>
        </div>
        <div class="row mt-4">
          <div class="table-responsive">
            <table class="table mb-0">
                <thead>
                    <tr>
                        <th width="10%">No</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Selesai</th>
                        <th>Nilai Akhir</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                  <?php $no=1?>
                  <?php foreach($loadExam->result() as $key){?>
                    <tr>
                      <td><?= $no?></td>
                      <td><?= $key->taken_date?></td>
                      <td><?= $key->modified_on?></td>
                      <td><?= $key->score?></td>
                      <?php if($key->score!=NULL){?>
                        <?php if($key->score<=$minimum_score){?>
                          <td><a href="<?= base_url()?>ujian/mulai/<?= $course_user_id?>" class="btn btn-primary">Perbaiki</a></td>
                        <?php }else{?>
                          <td>Selesai</td>
                        <?php }?>
                      <?php }else{?>
                        <td><a href="<?= base_url()?>ujian/mulai/<?= $course_user_id?>" class="btn btn-primary">Mulai Ujian</a></td>
                      <?php }?>
                    </tr>
                    <?php $no++?>
                  <?php }?>
                </tbody>
            </table>
        </div>
        </div>
        <div class="row mt-4">
            <div class="col-12">
                <div class="mb-4">
                    <br>
                    <h6 class="text-primary font-600">Catatan:</h6>
                    <ul class="list-style-none pl-0">
                        <li>
                            <p class="text-default mb-1">- Nilai akhir bisa diperbaiki dengan melakukan remedial
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
      <?php }else{ ?>
        <div class="row mt-4">
            <div class="col-12 col-md-8 offset-md-2">
                <h3 class="text-capitalize head-title text-center">Ujian Kelas</h3>
                <p class="head-sub-desc text-center">
                    Anda belum memiliki data ujian pada kelas ini <br><br>
                    <td><a href="<?= base_url()?>ujian/mulai/<?= $course_user_id?>" class="btn btn-primary">Mulai Ujian</a></td>
                    <!--   Kamu dapat mengakses kelas ini sampai tanggal <span class="text-danger font-400">30 Mei 2019 Pukul 23.59 WIB.</span> -->
                </p>
            </div>
        </div>

      <?php } ?>

    </div>
</section>

        
    </div>

    <script type="text/javascript" src="<?= base_url()?>vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/popper/popper.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/slick/js/slick.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/animsition/js/animsition.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets-ls/js/main.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets-ls/js/slick-add.js"></script>

</body>

</html>

