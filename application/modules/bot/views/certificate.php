<?php
    if($this->session->flashdata('message')) {
        $flashMessage=$this->session->flashdata('message');
        echo "<script>alert('$flashMessage')</script>";
    }
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="Demo project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sertifikat - Luar Sekolah</title>
    <link rel="shortcut icon" type="image/png" href="<?= base_url()?>assets-ls/images/icons/favicon.png" />
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>vendor/font-awesome/css/font-awesome.min.css">
    <!-- Slick -->
    <link id="effect" rel="stylesheet" type="text/css" media="all" href="<?= base_url()?>vendor/slick/css/slick.css" />
    <link rel="stylesheet" type="text/css" media="all" href="<?= base_url()?>vendor/slick/css/slick-theme.css" />
    <!-- Hamburgers -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>vendor/hamburgers/css/hamburgers.min.css">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets-ls/css/main.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets-ls/css/animate.css">
</head>

<body>
    <div class="navbar navbar-expand-lg desktop-navigation other-navbar fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">
                    <img src="<?= base_url()?>assets-ls/images/icons/logo.png" alt="" class="img-fluid logo-home">
                    <img src="<?= base_url()?>assets-ls/images/icons/logo-merah.png" alt="" class="img-fluid logo-other">
                </a>
            </div>
        </div>
    </div>

    <div class="content-box content-other">
    <section class="content-blank pt-3">
    <div class="container mb-4">

        <?php if($loadExam->num_rows()>0){?>
            <div class="row mt-4">
            <div class="col-12 col-md-8 offset-md-2">
                <h3 class="text-capitalize head-title text-center">Sertifikat Kelas</h3>
                <form action="" method="POST" name="form_review">
                    <div class="modal-body">
                    <label>Rating: (*)</label>
                    <div class="form-group">
                        <input type="number" name="rating" min="3" max="5" placeholder="5" class="form-control" required value="<?= $rating?>">
                    </div>
                    <label>Ulasan: (*)</label>
                    <div class="form-group">
                        <textarea class="form-control" name="rating_comment" minlength="5" maxlength="255" placeholder="Kelas dari Luarsekolah, memang luar biasa!" required=""><?= $rating_comment?></textarea>
                    </div>
                    </div>
                    <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-primary" data-dismiss="modal">Simpan</button> -->
                    <input type="submit" name="submit" class="btn btn-primary" value="UNDUH SERTIFIKAT" />
                    </div>
                </form>
            </div>
        </div>
        <?php }else{?>
            <div class="row mt-4">
            <div class="col-12 col-md-8 offset-md-2">
                <h3 class="text-capitalize head-title text-center">Sertifikat Kelas</h3>
                <p class="head-sub-desc text-center">
                    Anda belum melakukan ujian<br>Silakan untuk melakukan ujian terlebih dahulu.
                    <!--   Kamu dapat mengakses kelas ini sampai tanggal <span class="text-danger font-400">30 Mei 2019 Pukul 23.59 WIB.</span> -->
                </p>
            </div>
        </div>
        <?php }?>
        

    </div>
</section>

        
    </div>

    <script type="text/javascript" src="<?= base_url()?>vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/popper/popper.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/slick/js/slick.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>vendor/animsition/js/animsition.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets-ls/js/main.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets-ls/js/slick-add.js"></script>

</body>

</html>

