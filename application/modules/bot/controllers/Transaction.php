<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Transaction extends MY_Controller{
    public function __construct()
    {
		parent::__construct();
		$this->load->library('veritrans');
        $params = array('server_key' => 'SB-Mid-server-haIA5ziTIA1vlv_moAmadDgd', 'production' => FALSE);
		$this->load->helper('url');
		$this->load->model('Course_model');
		$this->load->model('Transaction_model');
		$this->veritrans->config($params);
    }
    public function create($data)
	{
		$mb=new MessageBuilder();
		$user_id=$data['user_id'];
		$course_id=$data['course_id'];
		$name=$data['name'];
		$email=$data['email'];
		// Delete course_user where payments null
		$this->Transaction_model->delete("course_user",array('user_id'=>$user_id,'payments_id'=>NULL));
		// Cek apakah user sudah membeli kelas
		$loadKelasUser=$this->Transaction_model->kelas_user($course_id,$user_id);
		if($loadKelasUser->num_rows()>0){//Jika user sudah membeli kelas
			if($loadKelasUser->row()->payment_status==1){
				return $mb->text("Anda sudah membeli kelas, silakan akses kelas di menu kelas saya");die;
			}else{
				return $mb->text("Anda sudah membeli kelas, silakan selesaikan pembayaran di menu riwayat pembelian");die;
			}
		}
		// Create Course User
		// Cek Harga setelah diskon
		$loadKelas=$this->Course_model->getCoursebyId($course_id)->row();
		if($loadKelas->duration<30){
			$durasi=$loadKelas->duration." Hari";
		  }else{
			$durasi=floor($loadKelas->duration/30);
			if($durasi==12){
			  $durasi="1 Tahun";
			}else{
			  $durasi=$durasi." Bulan";
			}
		  }
		$diskon=$loadKelas->discount;
		if($diskon<=100){
		  $price_cut=($diskon/100)*$loadKelas->price;
		}else{
		  $price_cut=$diskon;
		}
		$dataCourseUser=array(
            'user_id'=>$user_id,
            'course_id'=>$loadKelas->course_id,
            'duration'=>$loadKelas->duration,
            'price'=>($loadKelas->price-$price_cut)
          );
          // var_dump($dataCourseUser);die;
		  $insertCourseUser=$this->Transaction_model->insert($dataCourseUser,"course_user");
		  if($insertCourseUser){
			$course_user_id=$this->db->insert_id();
		  }
		  $order_id=uniqid();
		$transaction_details = array(
			'order_id' 			=> $order_id,
			'gross_amount' 	=> ($loadKelas->price-$price_cut)
		);

		// Populate items
        $items = [
            array(
                'id' 		=> $loadKelas->course_id,
                'price' 	=> ($loadKelas->price-$price_cut),
                'quantity' 	=> 1,
                'name' 		=> substr($loadKelas->subject,0,50)
            )
        ];

		// Populate customer's Info
		$customer_details = array(
			'first_name' 			=> "$name",
			'email' 				=> "$email",
			);

		// Data yang akan dikirim untuk request redirect_url.
		$transaction_data = array(
			'payment_type' 			=> 'vtweb',
			'vtweb' 				=> array(
				'credit_card_3d_secure' => true
			),
			'transaction_details'	=> $transaction_details,
			'item_details' 			=> $items,
			'customer_details' 	 	=> $customer_details
		);
		//TIMEZON php
		$tz = 'Asia/Jakarta';
		$timestamp = time();
		$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
		$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
		$datetime=$dt->format('Y-m-d H:i:s');
		$expiredtime = date('Y-m-d H:i:s',strtotime('+24 hour',strtotime($datetime)));
		$date=$dt->format('Y-m-d');
		//END TIMEZONE
		try
		{
			$vtweb_url = $this->veritrans->vtweb_charge($transaction_data);
			// Insert to payments
			$dataPayments=array(
				'snap_token'=>$vtweb_url,
				'midtrans_id'=>$order_id,
				'pay_amount'=>($loadKelas->price-$price_cut),
				'status'=>0,
				'pay_date'=>$datetime,
				'expire_date'=>$expiredtime
			);
			  // var_dump($dataCourseUser);die;
			  $insertPayments=$this->Transaction_model->insert($dataPayments,"payments");
			  if($insertPayments){
				$payments_id = $this->db->insert_id();
			  }
			  $dataUpdate=array('payments_id'=>$payments_id);
			  $updateCourseUser=$this->Transaction_model->update(array('course_user_id'=>$course_user_id),$dataUpdate,"course_user");

			
			$contents=array (
				'type' => 'bubble',
				'header' => 
				array (
				  'type' => 'box',
				  'layout' => 'vertical',
				  'flex' => 0,
				  'contents' => 
				  array (
					0 => 
					array (
					  'type' => 'box',
					  'layout' => 'baseline',
					  'margin' => 'md',
					  'contents' => 
					  array (
						0 => 
						array (
						  'type' => 'text',
						  'text' => $loadKelas->subject,
						  'size' => 'lg',
						  'align' => 'start',
						  'weight' => 'bold',
						  'wrap' => true,
						),
					  ),
					),
					1 => 
					array (
					  'type' => 'separator',
					  'margin' => 'md',
					),
					2 => 
					array (
					  'type' => 'box',
					  'layout' => 'horizontal',
					  'flex' => 2,
					  'margin' => 'sm',
					  'contents' => 
					  array (
						0 => 
						array (
						  'type' => 'text',
						  'text' => 'Kategori',
						  'flex' => 0,
						  'margin' => 'md',
						  'size' => 'md',
						  'align' => 'start',
						  'color' => '#999999',
						  'wrap' => true,
						),
						1 => 
						array (
						  'type' => 'text',
						  'text' => $loadKelas->category,
						  'align' => 'end',
						),
					  ),
					),
					3 => 
					array (
					  'type' => 'box',
					  'layout' => 'horizontal',
					  'flex' => 2,
					  'margin' => 'sm',
					  'contents' => 
					  array (
						0 => 
						array (
						  'type' => 'text',
						  'text' => 'Harga',
						  'flex' => 0,
						  'margin' => 'md',
						  'size' => 'md',
						  'align' => 'start',
						  'color' => '#999999',
						  'wrap' => true,
						),
						1 => 
						array (
						  'type' => 'text',
						  'text' => 'Rp.'.number_format(($loadKelas->price-$price_cut),0,',','.'),
						  'align' => 'end',
						),
					  ),
					),
					4 => 
					array (
					  'type' => 'box',
					  'layout' => 'horizontal',
					  'flex' => 2,
					  'margin' => 'sm',
					  'contents' => 
					  array (
						0 => 
						array (
						  'type' => 'text',
						  'text' => 'Durasi',
						  'flex' => 0,
						  'margin' => 'md',
						  'size' => 'md',
						  'align' => 'start',
						  'color' => '#999999',
						  'wrap' => true,
						),
						1 => 
						array (
						  'type' => 'text',
						  'text' => $durasi,
						  'align' => 'end',
						),
					  ),
					),
					5 => 
					array (
					  'type' => 'button',
					  'action' => 
					  array (
						'type' => 'uri',
						'label' => 'Midtrans',
						'uri' => $vtweb_url,
					  ),
					  'margin' => 'md',
					  'style' => 'primary',
					),
				  ),
				),
			);
			return $mb->flex("Midtrans",$contents);
		}
		catch (Exception $e)
		{
    		return $mb->text($e->getMessage());
		}

	}

    public function handler()
	{
		require('Bot.php');
		$bot=new Bot();
		echo 'test notification handler';
		$json_result = file_get_contents('php://input');
		$result = json_decode($json_result);

		if($result){
		$notif = $this->veritrans->status($result->order_id);
		}

		//notification handler sample

		$transaction = $notif->transaction_status;
		$type = $notif->payment_type;
		$order_id = $notif->order_id;

		if ($transaction == 'settlement'){
		  // TODO set payment status in merchant's database to 'Settlement'
		  $dataUpdate=array('status'=>1);
			  $updatePayments=$this->Transaction_model->update(array('midtrans_id'=>$order_id),$dataUpdate,"payments");
			  $getLineIdentity=$this->Transaction_model->getLineIdentity($order_id)->row();
			  $mb=new MessageBuilder();
			  $bot->push(array($mb->text("Pembayaran berhasil, selamat belajar!")),$getLineIdentity->line_identity);
			  redirect("https://line.me/R/oaMessage/@293mfwar/?%20");
		  }
		  else if($transaction == 'pending'){
		  // TODO set payment status in merchant's database to 'Pending'
		  echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
		  }
		  else if ($transaction == 'deny') {
		  // TODO set payment status in merchant's database to 'Denied'
		  echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
		}

	}
    function delete(){
        
    }
    function history($user_id){
		$mb=new MessageBuilder();
		$history=$this->Transaction_model->historyData($user_id);
		if($history->num_rows()==0){
			return $mb->text("Anda belum melakukan pembelian");
		}else{
			$itemsHistory=[];
			foreach($history->result() as $key){
				if($key->duration<30){
					$durasi=$key->duration." Hari";
				  }else{
					$durasi=floor($key->duration/30);
					if($durasi==12){
					  $durasi="1 Tahun";
					}else{
					  $durasi=$durasi." Bulan";
					}
				  }
				  if($key->code==NULL){
					  $metode="Midtrans";
					  $keterangan="Status";
					  if($key->status==1){
						$action=array (
							'type' => 'postback',
							'label' => 'LUNAS',
							'data' => 'blank',
						  );
					  }else{
						  if($key->snap_token!=NULL){
							$action=array (
								'type' => 'uri',
								'label' => 'PENDING',
								'uri' => $key->snap_token,
							  ); 
						  }else{
							$action=array (
								'type' => 'postback',
								'label' => 'PENDING',
								'data' => 'blank',
							  );
						  }
						
					  }
					  
				  }else{
					  $metode="Voucher";
					  $keterangan="Kode Voucher";
					  $action=array (
						'type' => 'postback',
						'label' => $key->code,
						'data' => 'blank',
					  );
				  }
				$itemHistory=array (
					'type' => 'bubble',
					'header' => 
					array (
					  'type' => 'box',
					  'layout' => 'vertical',
					  'flex' => 0,
					  'contents' => 
					  array (
						0 => 
						array (
						  'type' => 'text',
						  'text' => 'Riwayat Pesanan',
						  'size' => 'lg',
						  'align' => 'center',
						  'gravity' => 'center',
						  'weight' => 'bold',
						),
						1 => 
						array (
						  'type' => 'separator',
						  'margin' => 'md',
						),
						2 => 
						array (
						  'type' => 'box',
						  'layout' => 'baseline',
						  'margin' => 'md',
						  'contents' => 
						  array (
							0 => 
							array (
							  'type' => 'text',
							  'text' => substr($key->subject,0,50),
							  'size' => 'md',
							  'align' => 'start',
							  'weight' => 'regular',
							  'wrap' => true,
							),
						  ),
						),
						3 => 
						array (
						  'type' => 'box',
						  'layout' => 'horizontal',
						  'flex' => 2,
						  'margin' => 'sm',
						  'contents' => 
						  array (
							0 => 
							array (
							  'type' => 'text',
							  'text' => 'Kategori',
							  'flex' => 0,
							  'margin' => 'md',
							  'size' => 'md',
							  'align' => 'start',
							  'color' => '#999999',
							  'wrap' => true,
							),
							1 => 
							array (
							  'type' => 'text',
							  'text' => $key->category,
							  'align' => 'end',
							),
						  ),
						),
						4 => 
						array (
						  'type' => 'box',
						  'layout' => 'horizontal',
						  'flex' => 2,
						  'margin' => 'sm',
						  'contents' => 
						  array (
							0 => 
							array (
							  'type' => 'text',
							  'text' => 'Harga',
							  'flex' => 0,
							  'margin' => 'md',
							  'size' => 'md',
							  'align' => 'start',
							  'color' => '#999999',
							  'wrap' => true,
							),
							1 => 
							array (
							  'type' => 'text',
							  'text' => 'Rp. '.number_format(($key->price),0,',','.'),
							  'align' => 'end',
							),
						  ),
						),
						5 => 
						array (
						  'type' => 'box',
						  'layout' => 'horizontal',
						  'flex' => 2,
						  'margin' => 'sm',
						  'contents' => 
						  array (
							0 => 
							array (
							  'type' => 'text',
							  'text' => 'Durasi',
							  'flex' => 0,
							  'margin' => 'md',
							  'size' => 'md',
							  'align' => 'start',
							  'color' => '#999999',
							  'wrap' => true,
							),
							1 => 
							array (
							  'type' => 'text',
							  'text' => $durasi,
							  'align' => 'end',
							),
						  ),
						),
						6 => 
						array (
						  'type' => 'box',
						  'layout' => 'horizontal',
						  'flex' => 2,
						  'margin' => 'sm',
						  'contents' => 
						  array (
							0 => 
							array (
							  'type' => 'text',
							  'text' => 'Metode',
							  'flex' => 0,
							  'margin' => 'md',
							  'size' => 'md',
							  'align' => 'start',
							  'color' => '#999999',
							  'wrap' => true,
							),
							1 => 
							array (
							  'type' => 'text',
							  'text' => $metode,
							  'align' => 'end',
							),
						  ),
						),
						7 => 
						array (
						  'type' => 'text',
						  'text' => $keterangan,
						  'margin' => 'md',
						  'align' => 'center',
						),
						8 => 
						array (
						  'type' => 'button',
						  'action' => $action,
						  'margin' => 'md',
						  'style' => 'primary',
						),
					  ),
					),
				);
				array_push($itemsHistory,$itemHistory);
			}
			$contents=array (
				'type' => 'carousel',
				'contents' => $itemsHistory
			);
			return $mb->flex("Riwayat Pembelian",$contents);
		}
		
    }
    function finish(){
        redirect("https://line.me/R/oaMessage/@293mfwar/?%20");
    }
}
?>