<?php 
class Register extends MY_Controller{
    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this
            ->load
            ->model(array(
            'Register_model'
        ));

    }
    function sendFlex($line_identity){
        $result=array (
            'type' => 'bubble',
            'header' => 
            array (
              'type' => 'box',
              'layout' => 'vertical',
              'flex' => 0,
              'contents' => 
              array (
                0 => 
                array (
                  'type' => 'button',
                  'action' => 
                  array (
                    'type' => 'uri',
                    'label' => 'DAFTAR',
                    'uri' => base_url().'bot/register?line_identity='.$line_identity,
                  ),
                  'color' => '#00CC99',
                  'margin' => 'lg',
                  'style' => 'primary',
                )
              ),
            ),
        );
        return $result;

    }
    function index(){
        $line_identity=$this->input->get('line_identity');
        $data['line_identity']=$line_identity;
        $this->load->view('register',$data);
    }

    function action(){
        require('Bot.php');
        $mb=new MessageBuilder();
        $bot=new Bot();
        $fullname=$this->input->post('fullname');
        $email=$this->input->post("email");
        $password=$this->input->post("password");
        $password = password_hash($password, PASSWORD_BCRYPT);
        $job=$this->input->post("job");
        $gender=$this->input->post('gender');
        $birthdate=$this->input->post('birthdate');
        $line_identity=$this->input->post('line_identity');
        $where=array('email'=>$email);
        $getData=$this->Register_model->checkData($where);
        if($getData->num_rows()>0){
            $bot->push(array($mb->text("Email sudah terdaftar, silakan login")),$line_identity);
            redirect("https://line.me/R/oaMessage/@293mfwar/?%20");
        }else{
            $dataInsert=array(
                'email'=>$email,
                'fullname'=>$fullname,
                'job'=>$job,
                'birthdate'=>$birthdate,
                'gender'=>$gender,
                'password'=>$password
            );
            $insertData=$this->Register_model->insertData($dataInsert);
            if($insertData){
                $bot->push(array($mb->text("Pendaftaran berhasil, silakan login")),$line_identity);
                redirect("https://line.me/R/oaMessage/@293mfwar/?%20");
            }
        }
    }
}