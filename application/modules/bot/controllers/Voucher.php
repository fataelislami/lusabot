<?php
class Voucher extends MY_Controller{
    public function __construct()
    {
		parent::__construct();
        $this->load->model('Voucher_model');
        $this->load->model('Users_model');
        $this->load->model('Course_model');
        $this->load->model('Transaction_model');
    }
    function getVoucher($kode_voucher){
        $mb=new MessageBuilder();
        $getData=$this->Voucher_model->checkData(array('code'=>$kode_voucher));
        if($getData->num_rows()>0){
            $getData=$getData->row();
            if($getData->duration<30){
                $durasi=$getData->duration." Hari";
              }else{
                $durasi=floor($getData->duration/30);
                if($durasi==12){
                  $durasi="1 Tahun";
                }else{
                  $durasi=$durasi." Bulan";
                }
              }
              $diskon=$getData->discount;
              if($diskon<=100){
                $price_cut=($diskon/100)*$getData->price;
              }else{
                $price_cut=$diskon;
              }
              $subject=$getData->subject;
              $category=$getData->category;
              $code=$getData->code;
            $content=array (
                'type' => 'bubble',
                'header' => 
                array (
                  'type' => 'box',
                  'layout' => 'vertical',
                  'flex' => 0,
                  'contents' => 
                  array (
                    0 => 
                    array (
                      'type' => 'text',
                      'text' => 'Ringkasan',
                      'size' => 'lg',
                      'align' => 'center',
                      'gravity' => 'center',
                      'weight' => 'bold',
                    ),
                    1 => 
                    array (
                      'type' => 'separator',
                      'margin' => 'md',
                    ),
                    2 => 
                    array (
                      'type' => 'box',
                      'layout' => 'baseline',
                      'margin' => 'md',
                      'contents' => 
                      array (
                        0 => 
                        array (
                          'type' => 'text',
                          'text' => $subject,
                          'size' => 'md',
                          'align' => 'start',
                          'weight' => 'regular',
                          'wrap' => true,
                        ),
                      ),
                    ),
                    3 => 
                    array (
                      'type' => 'box',
                      'layout' => 'horizontal',
                      'flex' => 2,
                      'margin' => 'sm',
                      'contents' => 
                      array (
                        0 => 
                        array (
                          'type' => 'text',
                          'text' => 'Kategori',
                          'flex' => 0,
                          'margin' => 'md',
                          'size' => 'md',
                          'align' => 'start',
                          'color' => '#999999',
                          'wrap' => true,
                        ),
                        1 => 
                        array (
                          'type' => 'text',
                          'text' => $category,
                          'align' => 'end',
                        ),
                      ),
                    ),
                    4 => 
                    array (
                      'type' => 'box',
                      'layout' => 'horizontal',
                      'flex' => 2,
                      'margin' => 'sm',
                      'contents' => 
                      array (
                        0 => 
                        array (
                          'type' => 'text',
                          'text' => 'Harga',
                          'flex' => 0,
                          'margin' => 'md',
                          'size' => 'md',
                          'align' => 'start',
                          'color' => '#999999',
                          'wrap' => true,
                        ),
                        1 => 
                        array (
                          'type' => 'text',
                          'text' => 'Rp. '.number_format(($getData->price-$price_cut),0,',','.'),
                          'align' => 'end',
                        ),
                      ),
                    ),
                    5 => 
                    array (
                      'type' => 'box',
                      'layout' => 'horizontal',
                      'flex' => 2,
                      'margin' => 'sm',
                      'contents' => 
                      array (
                        0 => 
                        array (
                          'type' => 'text',
                          'text' => 'Durasi',
                          'flex' => 0,
                          'margin' => 'md',
                          'size' => 'md',
                          'align' => 'start',
                          'color' => '#999999',
                          'wrap' => true,
                        ),
                        1 => 
                        array (
                          'type' => 'text',
                          'text' => $durasi,
                          'align' => 'end',
                        ),
                      ),
                    ),
                    6 => 
                    array (
                      'type' => 'box',
                      'layout' => 'horizontal',
                      'flex' => 2,
                      'margin' => 'sm',
                      'contents' => 
                      array (
                        0 => 
                        array (
                          'type' => 'text',
                          'text' => 'Metode',
                          'flex' => 0,
                          'margin' => 'md',
                          'size' => 'md',
                          'align' => 'start',
                          'color' => '#999999',
                          'wrap' => true,
                        ),
                        1 => 
                        array (
                          'type' => 'text',
                          'text' => 'Voucher',
                          'align' => 'end',
                        ),
                      ),
                    ),
                    7 => 
                    array (
                      'type' => 'text',
                      'text' => 'Kode Voucher',
                      'margin' => 'md',
                      'align' => 'center',
                    ),
                    8 => 
                    array (
                      'type' => 'button',
                      'action' => 
                      array (
                        'type' => 'postback',
                        'label' => $code,
                        'data' => 'lorem ipsum dolor',
                      ),
                      'color' => '#E5E5E5',
                      'margin' => 'sm',
                      'height' => 'sm',
                      'style' => 'secondary',
                    ),
                    9 => 
                    array (
                      'type' => 'box',
                      'layout' => 'horizontal',
                      'margin' => 'lg',
                      'contents' => 
                      array (
                        0 => 
                        array (
                          'type' => 'button',
                          'action' => 
                          array (
                            'type' => 'postback',
                            'label' => 'Tidak',
                            'data' => 'redeem#tidak#'.$code,
                          ),
                        ),
                        1 => 
                        array (
                          'type' => 'button',
                          'action' => 
                          array (
                            'type' => 'postback',
                            'label' => 'Ya',
                            'data' => 'redeem#ya#'.$code,
                          ),
                          'margin' => 'none',
                          'style' => 'primary',
                        ),
                      ),
                    ),
                  ),
                ),
            );
          return $mb->flex("Konfirmasi Voucher",$content);
        }else{
            return $mb->text("Voucher tidak ditemukan, silakan ulangi atau ketik RESET untuk kembali");
        }
    }
    function updateFlag($line_identity){
        $mb=new MessageBuilder();
        $data=array('flag'=>'voucher');
        $updateData=$this->Users_model->update(array('line_identity'=>$line_identity),$data,'users');
        if($updateData){
          return $mb->text("Silakan masukan kode voucher disini");
        }else{
          return $mb->text("Proses gagal");
          $data=array('flag'=>NULL);
          $updateData=$this->Users_model->update(array('line_identity'=>$line_identity),$data,'users');
        }
    }
      function resetFlag($line_identity){
        $mb=new MessageBuilder();
        $data=array('flag'=>NULL);
        $updateData=$this->Users_model->update(array('line_identity'=>$line_identity),$data,'users');
        if($updateData){
          return $mb->text("Kamu telah keluar dari sesi redeem voucher");
        }else{
          return $mb->text("Proses gagal");
          $data=array('flag'=>NULL);
          $updateData=$this->Users_model->update(array('line_identity'=>$line_identity),$data,'users');
        }
    }

    function redeem($code,$user_id){
        $mb=new MessageBuilder();
        // Cek apakah kode voucher sudah terpakai
        $getDataVoucher=$this->Voucher_model->checkData(array('code'=>$code,'is_used'=>0));
        if($getDataVoucher->num_rows()>0){
            $getDataVoucher=$getDataVoucher->row();
            // ambil data kelas
            $loadKelas=$this->Course_model->getCoursebyId($getDataVoucher->course_id)->row();
            // 
            $durasi=$loadKelas->duration;
            $diskon=$loadKelas->discount;
            if($diskon<=100){
              $price_cut=($diskon/100)*$loadKelas->price;
            }else{
              $price_cut=$diskon;
            }
            $price=($loadKelas->price-$price_cut);
            //TIMEZON php
            $tz = 'Asia/Jakarta';
            $timestamp = time();
            $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
            $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
            $datetime=$dt->format('Y-m-d H:i:s');
            $date=$dt->format('Y-m-d');
            //END TIMEZONE
            // Insert to payments
            $dataPayments=array(
                'pay_amount'=>$price,
                'status'=>1,
                'pay_date'=>$datetime,
                'voucher_id'=>$getDataVoucher->vouchers_id
            );
            // var_dump($dataCourseUser);die;
            $insertPayments=$this->Transaction_model->insert($dataPayments,"payments");
            if($insertPayments){
            $payments_id = $this->db->insert_id();
            }
            $dataCourseUser=array(
                'user_id'=>$user_id,
                'course_id'=>$loadKelas->course_id,
                'duration'=>$durasi,
                'price'=>($loadKelas->price-$price_cut),
                'payments_id'=>$payments_id
              );
              // var_dump($dataCourseUser);die;
              $insertCourseUser=$this->Transaction_model->insert($dataCourseUser,"course_user");
              if($insertCourseUser){
                  $data=array('is_used'=>1);
                  $updateVoucher=$this->Voucher_model->update(array('code'=>$code),$data,'vouchers');
                return $mb->text("Voucher berhasil di redeem, selamat belajar!");
              }
        }else{
            return $mb->text("Voucher sudah digunakan\nSesi redeem telah di reset");
        }
    }
}