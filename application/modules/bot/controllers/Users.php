<?php
class Users extends MY_Controller{
    public function __construct()
    {
		parent::__construct();
    $this->load->model('Users_model');
    $this->load->helper('tanggal');
    }
    
    function profile($line_identity=NULL){
      $this->load->model("Login_model");
      $checkData=$this->Login_model->checkData(array('line_identity'=>$line_identity));
      if($checkData->num_rows()>0){
        $getData=$checkData->row();
      }else{
        redirect("https://line.me/R/oaMessage/@293mfwar/?%20");
      }
      $data['line_identity']=$line_identity;
      $data['getData']=$getData;
      $this->load->view('profile',$data);
    }
    function profile_action(){
      $fullname=$this->input->post('fullname');
      $email=$this->input->post("email");
      $job=$this->input->post("job");
      $gender=$this->input->post('gender');
      $birthdate=$this->input->post('birthdate');
      $line_identity=$this->input->post('line_identity');
      $password=$this->input->post("password");
      $dataUpdate=array(
        'fullname'=>$fullname,
        'job'=>$job,
        'birthdate'=>$birthdate,
        'gender'=>$gender,
        'line_identity'=>$line_identity
    );
    if($password!=''){
      $dataUpdate['password']=password_hash($password, PASSWORD_BCRYPT);
    }
    $updateUser=$this->Users_model->update(array('email'=>$email),$dataUpdate,'users');
    if($updateUser){
      $this->session->set_flashdata('message', 'Profil berhasil di ubah');
      redirect(base_url()."bot/users/profile/".$line_identity);
    }else{
      $this->session->set_flashdata('message', 'Profil gagal di ubah');
      redirect(base_url()."bot/users/profile/".$line_identity);
    }
    }
    function sendProfile($line_identity,$displayPicture){
      $mb=new MessageBuilder();
      $this->load->model("Login_model");
      $checkData=$this->Login_model->checkData(array('line_identity'=>$line_identity));
      if($checkData->num_rows()>0){
        $getData=$checkData->row();
      }else{
        return $mb->text("Profile tidak ditemukan");
      }
      $content=array (
        'type' => 'bubble',
        'header' => 
        array (
          'type' => 'box',
          'layout' => 'vertical',
          'flex' => 0,
          'contents' => 
          array (
            0 => 
            array (
              'type' => 'image',
              'url' => $displayPicture,
              'margin' => 'md',
              'align' => 'center',
              'gravity' => 'center',
              'size' => 'full',
              'aspectRatio' => '1.51:1',
              'aspectMode' => 'cover',
              'backgroundColor' => '#FFFFFF',
            ),
            1 => 
            array (
              'type' => 'box',
              'layout' => 'baseline',
              'margin' => 'md',
              'contents' => 
              array (
                0 => 
                array (
                  'type' => 'text',
                  'text' => $getData->fullname,
                  'size' => 'lg',
                  'align' => 'start',
                  'weight' => 'bold',
                  'wrap' => true,
                ),
              ),
            ),
            2 => 
            array (
              'type' => 'box',
              'layout' => 'horizontal',
              'flex' => 2,
              'margin' => 'sm',
              'contents' => 
              array (
                0 => 
                array (
                  'type' => 'text',
                  'text' => 'Jenis Kelamin',
                  'flex' => 0,
                  'margin' => 'md',
                  'size' => 'md',
                  'align' => 'start',
                  'color' => '#999999',
                  'wrap' => true,
                ),
                1 => 
                array (
                  'type' => 'text',
                  'text' => ($getData->gender=='male'?'Laki-laki':'Perempuan'),
                  'align' => 'end',
                ),
              ),
            ),
            3 => 
            array (
              'type' => 'box',
              'layout' => 'horizontal',
              'flex' => 2,
              'margin' => 'sm',
              'contents' => 
              array (
                0 => 
                array (
                  'type' => 'text',
                  'text' => 'Pekerjaan',
                  'flex' => 0,
                  'margin' => 'md',
                  'size' => 'md',
                  'align' => 'start',
                  'color' => '#999999',
                  'wrap' => true,
                ),
                1 => 
                array (
                  'type' => 'text',
                  'text' => $getData->job,
                  'align' => 'end',
                ),
              ),
            ),
            4 => 
            array (
              'type' => 'box',
              'layout' => 'horizontal',
              'flex' => 2,
              'margin' => 'sm',
              'contents' => 
              array (
                0 => 
                array (
                  'type' => 'text',
                  'text' => 'Umur',
                  'flex' => 0,
                  'margin' => 'md',
                  'size' => 'md',
                  'align' => 'start',
                  'color' => '#999999',
                  'wrap' => true,
                ),
                1 => 
                array (
                  'type' => 'text',
                  'text' => '23',
                  'align' => 'end',
                ),
              ),
            ),
            5 => 
            array (
              'type' => 'button',
              'action' => 
              array (
                'type' => 'uri',
                'label' => 'Kelola Akun',
                'uri' => base_url()."bot/users/profile/".$line_identity,
              ),
              'margin' => 'md',
              'style' => 'primary',
            ),
          ),
        ),
      );
      $result=$mb->flex("Profile Saya",$content);
      return $result;
    }
    function getCourse($user_id){
        $mb=new MessageBuilder();
        $course=$this->Users_model->getCourse($limit=9,$offset=0,$user_id,$finish=0);
        if($course->num_rows()==0){
            return $mb->text("Anda belum memiliki kelas");
        }else{
            $itemsKelas=[];
            foreach($course->result() as $key){
                $rating=round(($key->rata_rata!=NULL?$key->rata_rata:0),1);
                $itemKelas=array (
                    'type' => 'bubble',
                    'direction' => 'ltr',
                    'hero' => 
                    array (
                      'type' => 'image',
                      'url' => 'https://cdnls.ams3.digitaloceanspaces.com/imagebot/course/'.urlencode($key->image),
                      'size' => 'full',
                      'aspectRatio' => '20:13',
                      'aspectMode' => 'cover',
                    ),
                    'body' => 
                    array (
                      'type' => 'box',
                      'layout' => 'vertical',
                      'contents' => 
                      array (
                        0 => 
                        array (
                          'type' => 'text',
                          'text' => $key->subject,
                          'size' => 'md',
                          'weight' => 'bold',
                          'wrap' => true,
                        ),
                        1 => 
                        array (
                          'type' => 'box',
                          'layout' => 'baseline',
                          'margin' => 'md',
                          'contents' => 
                          array (
                            0 => 
                            array (
                              'type' => 'icon',
                              'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png',
                              'size' => 'sm',
                            ),
                            1 => 
                            array (
                              'type' => 'icon',
                              'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png',
                              'size' => 'sm',
                            ),
                            2 => 
                            array (
                              'type' => 'icon',
                              'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png',
                              'size' => 'sm',
                            ),
                            3 => 
                            array (
                              'type' => 'icon',
                              'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png',
                              'size' => 'sm',
                            ),
                            4 => 
                            array (
                              'type' => 'icon',
                              'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png',
                              'size' => 'sm',
                            ),
                            5 => 
                            array (
                              'type' => 'text',
                              'text' => "$rating",
                              'flex' => 0,
                              'margin' => 'md',
                              'size' => 'sm',
                              'color' => '#999999',
                            ),
                          ),
                        ),
                        2 => 
                        array (
                          'type' => 'box',
                          'layout' => 'vertical',
                          'spacing' => 'sm',
                          'margin' => 'lg',
                          'contents' => 
                          array (
                            0 => 
                            array (
                              'type' => 'box',
                              'layout' => 'vertical',
                              'spacing' => 'sm',
                              'contents' => 
                              array (
                                0 => 
                                array (
                                  'type' => 'text',
                                  'text' => $key->category,
                                  'size' => 'md',
                                  'color' => '#AAAAAA',
                                ),
                              ),
                            ),
                            1 => 
                            array (
                              'type' => 'box',
                              'layout' => 'horizontal',
                              'spacing' => 'lg',
                              'margin' => 'md',
                              'contents' => 
                              array (
                                0 => 
                                array (
                                  'type' => 'button',
                                  'action' => 
                                  array (
                                    'type' => 'postback',
                                    'label' => 'Belajar',
                                    'data' => 'belajar#'.$key->course_id,
                                  ),
                                  'color' => '#FA4E4E',
                                  'style' => 'primary',
                                ),
                                1 => 
                                array (
                                  'type' => 'button',
                                  'action' => 
                                  array (
                                    'type' => 'uri',
                                    'label' => 'Ujian',
                                    'uri' => base_url()."ujian/".$key->course_user_id,
                                  ),
                                  'color' => '#FA4E4E',
                                  'style' => 'primary',
                                ),
                              ),
                            ),
                            2 => 
                            array (
                              'type' => 'box',
                              'layout' => 'vertical',
                              'spacing' => 'lg',
                              'margin' => 'lg',
                              'contents' => 
                              array (
                                0 => 
                                array (
                                  'type' => 'button',
                                  'action' => 
                                  array (
                                    'type' => 'uri',
                                    'label' => 'Sertifikat',
                                    'uri' => base_url()."sertifikat/".$key->course_user_id,
                                  ),
                                  'color' => '#FA4E4E',
                                  'style' => 'primary',
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                  array_push($itemsKelas,$itemKelas);
            }
            $contents=array (
                'type' => 'carousel',
                'contents' => $itemsKelas
            );
            return $mb->flex("Kelas Saya",$contents);
        }

    }
    function certificate($course_user_id){
      $this->load->model('Exam_model');
      $loadExam=$this->Exam_model->getExamUser($course_user_id);
      $loadCourseUser=$this->Users_model->course_user($course_user_id);
      if($loadCourseUser->num_rows()>0){
        $loadCourseUser=$loadCourseUser->row();
        $data['rating']=$loadCourseUser->rating;
        $data['rating_comment']=$loadCourseUser->rating_comment;
      }else{
          $data['rating']=null;
          $data['rating_comment']=null;
      }
      $data['loadExam']=$loadExam;
      $data['course_user_id']=$course_user_id;
      if($this->input->post('rating')!=''){
        // Simpan data rating
        $rating=$this->input->post('rating');
        $rating_comment=$this->input->post('rating_comment');
        $dataUpdate=array(
          'rating'=>$rating,
          'rating_comment'=>$rating_comment
        );
        $updateRating=$this->Users_model->update(array('course_user_id'=>$course_user_id),$dataUpdate,'course_user');
        // Buat sertifikat disini
        $dataSertifikat=$this->Users_model->getDataCertificate($course_user_id);
        if($dataSertifikat->num_rows()==0){
          redirect(redirect($_SERVER['HTTP_REFERER']));
        }

        //Init Data BEGIN
        $dataSertifikat=$dataSertifikat->row();
        $nama_pelanggan=$dataSertifikat->fullname;
        $nama_kreator=$dataSertifikat->creator;
        $tanda_tangan_kreator=$dataSertifikat->signature;
        $judul_kelas=$dataSertifikat->subject;
        if($dataSertifikat->score>=80){
          $nilaiAkhir="A";
        }
        else if($dataSertifikat->score>=70 AND $dataSertifikat->score<80){
          $nilaiAkhir="B";
        }
        else{
          $nilaiAkhir="C";
        }
        //tanggal
        $tanggal=formatHariTanggal($dataSertifikat->taken_date);
        $exTanggalheader=explode("-",$tanggal);
        $tanggalHeader=$exTanggalheader[0]."/".$exTanggalheader[1].'-'.$exTanggalheader[2];
        ///Init Data END
        
        $this->load->library('kostlabpdf');
        $this->kostlabpdf->generate();
        $pdf = new FPDF('L','mm','A4');
        $image1 = "https://luarsekolah.com/assets-front/images/certificate-new.png";

        //add new freescript font
        $pdf->AddFont('Comforta','','Comforta.php');

        //add new jokerman font
        $pdf->AddFont('Sourcesans','','SourceSansPro-Regular.php');


        $pdf->AddPage();
        $pdf->Image($image1, 0, 0, 297);

        //Training
        // $pdf->Cell(0,10,'Left text',0,0,'L');
        // $pdf->SetX($pdf->lMargin);
        // $pdf->Cell(0,10,'Center text:',0,0,'C');
        // $pdf->SetX($pdf->lMargin);
        // $pdf->Cell( 0, 10, 'Right text', 0, 0, 'R' );
        $pdf->SetFont('Comforta','',18);
        $pdf->SetXY(45, 60);
        $pdf->SetTextColor(0,204,153);
        $pdf->Cell( 0, 0, 'no.'.$dataSertifikat->id.'/sert-of-ls'.$tanggalHeader, 0, 0, 'L' );
        $pdf->SetFont('Sourcesans','',18);
        $pdf->SetXY(45, 85);
        $pdf->SetTextColor(87,86,86);
        $pdf->Cell( 0, 0, 'Diberikan Kepada:', 0, 0, 'L' );
        $pdf->SetFont('Comforta','',40);
        $pdf->SetXY(45, 99);
        $pdf->SetTextColor(0,204,153);
        $pdf->Cell( 0, 0, $nama_pelanggan, 0, 0, 'L' );
        $pdf->SetFont('Sourcesans','',18);
        $pdf->SetXY(45, 112);
        $pdf->SetTextColor(87,86,86);
        $pdf->Cell( 0, 0, 'pada tanggal '.$tanggal.' telah berhasil', 0, 0, 'L' );
        $pdf->SetFont('Sourcesans','',18);
        $pdf->SetXY(45, 122);
        $pdf->SetTextColor(87,86,86);
        $pdf->Cell( 0, 0, 'menyelesaikan kelas online:', 0, 0, 'L' );
        $pdf->SetFont('Comforta','',18);
        $pdf->SetXY(45, 130);
        $pdf->SetTextColor(0,204,153);
        $pdf->Cell( 0, 0, $judul_kelas, 0, 0, 'L' );
        $pdf->SetFont('Comforta','',18);
        $pdf->SetXY(45, 138);
        $pdf->SetTextColor(87,86,86);
        $pdf->Cell( 0, 0, 'dengan memperoleh nilai akhir '.$nilaiAkhir, 0, 0, 'L' );
        $pdf->SetFont('Comforta','',14);
        $pdf->SetXY(208, 182);
        $pdf->SetTextColor(0,204,153);
        $pdf->Cell( 0, 0, $nama_kreator, 0, 0, 'L' );
        $pdf->SetFont('Comforta','',10);
        $pdf->SetXY(160, 187);
        $pdf->SetTextColor(0,204,153);
        $pdf->Cell( 0, 0, 'Kreator', 0, 0, 'C' );
        if($tanda_tangan_kreator=='' || $tanda_tangan_kreator==NULL)
        {
          $this->session->set_flashdata('message', 'Tanda tangan kosong');
          redirect(redirect($_SERVER['HTTP_REFERER']));
        }
        else
        {
          $signature_file = 'https://cdnls.ams3.digitaloceanspaces.com/imagebot/signature/'.$tanda_tangan_kreator;
          // var_dump($signature_file); die;
        }
        $pdf->Image('https://cdnls.ams3.digitaloceanspaces.com/imagebot/signature/'.$tanda_tangan_kreator, 202, 152, 50);

        $pdf->Output();

        // $this->load->view('generate/sertifikat/kelas_online');
      }else{
        $this->load->view('bot/certificate',$data);
      }
    }
}
