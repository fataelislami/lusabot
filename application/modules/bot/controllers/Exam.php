<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exam extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model('Exam_model');    
  }

  function list($course_user_id){
    $loadExam=$this->Exam_model->getExamUser($course_user_id);
    $data['minimum_score']=70;
    $data['loadExam']=$loadExam;
    $data['course_user_id']=$course_user_id;
    $this->load->view('bot/exam/list',$data);
  }

  function start($course_user_id){
        //TIMEZON php
		$tz = 'Asia/Jakarta';
		$timestamp = time();
		$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
		$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
		$datetime=$dt->format('Y-m-d H:i:s');
		$date=$dt->format('Y-m-d');
		//END TIMEZONE
    $data_insert=array(
      'course_user_id'=>$course_user_id,
      'taken_date'=>$datetime
    );
    $course_id=$this->Exam_model->getCourseId($course_user_id);
    // Cek data ujian
    $this->db->where('score',NULL);
    $loadExam=$this->Exam_model->getExamUser($course_user_id);
    if($loadExam->num_rows()>0){
      $loadExam=$loadExam->row();
      $exam_user_id=$loadExam->exam_user_id;
    }else{
      $insertData=$this->Exam_model->insert($data_insert,"exam_user");
      if($insertData){
        $exam_user_id=$this->db->insert_id();
      }
    }
    $this->multiple_choice($course_id,$course_user_id,$exam_user_id);

  }


  function multiple_choice($course_id,$course_users_id,$exam_user_id){
    $this->load->helper(array('dbs'));

    $loadExam=$this->Exam_model->getMultiple($course_id);
    // var_dump($loadExam->result());die;
    $data = array(
      'otherpages'=>true,
    );
    $data['course_id']=$course_id;
    $data['course_user_id']=$course_users_id;
    $data['loadExam']=$loadExam;
    $data['exam_user_id']=$exam_user_id;
    $this->load->view('bot/exam/multiple',$data);
  }

  function multiple_action(){
    $this->load->helper(array('dbs'));
    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $datetime=$dt->format('Y-m-d H:i:s');
    $date=$dt->format('Y-m-d');
    //END TIMEZONE
    $jawaban=$this->input->post('jawaban');
    $totalNomor=$this->input->post('totalNomor');
    $totalNomor=($totalNomor-1);
    $exam_user_id=$this->input->post('exam_user_id');
    $course_user_id=$this->input->post('course_user_id');
    $course_id=$this->input->post('course_id');
    $totalPoint=0;
    for ($i=0; $i <$totalNomor; $i++) {
      if($this->input->post('pilihan'.($i+1))==$jawaban[$i]){
        $totalPoint=$totalPoint+1;
      }
    }
   $totalScore=($totalPoint/$totalNomor)*100;
   $dataUpdate=array(
     'score'=>$totalScore,
     'modified_on'=>$datetime
   );
   $sql=updateData($dataUpdate,'exam_user',array('exam_user_id'=>$exam_user_id));
   if($sql){
     redirect(base_url('ujian/'.$course_user_id));
   }
  }

}
