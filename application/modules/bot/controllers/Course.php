<?php 
require_once('MessageBuilder.php');
class Course extends MY_Controller{
    function getListCourse(){
        $messageBuilder=new MessageBuilder();
        $this->load->model('Course_model');
        $getList=$this->Course_model->getAllData();
        $itemKelasArr=[];
        if($getList->num_rows()>0){
          var_dump($getList->result());
          $i=1;
          foreach($getList->result() as $key){
            $rating=round(($key->rata_rata!=NULL?$key->rata_rata:0),1);
            $itemKelas=array(
              'type' => 'bubble',
              'direction' => 'ltr',
              'hero' => 
              array (
                'type' => 'image',
                'url' => 'https://cdnls.ams3.digitaloceanspaces.com/imagebot/course/'.urlencode($key->image),
                'size' => 'full',
                'aspectRatio' => '20:13',
                'aspectMode' => 'cover',
              ),
              'body' => 
              array (
                'type' => 'box',
                'layout' => 'vertical',
                'contents' => 
                array (
                  0 => 
                  array (
                    'type' => 'text',
                    'text' => $key->subject,
                    'size' => 'lg',
                    'weight' => 'bold',
                    'wrap'=>true
                  ),
                  1 => 
                  array (
                    'type' => 'text',
                    'text' => 'Rp.'.number_format($key->price,0,',','.'),
                    'margin' => 'lg',
                    'size' => 'md',
                    'align' => 'start',
                    'gravity' => 'center',
                    'weight' => 'bold',
                  ),
                  2 => 
                  array (
                    'type' => 'box',
                    'layout' => 'baseline',
                    'margin' => 'md',
                    'contents' => 
                    array (
                      0 => 
                      array (
                        'type' => 'icon',
                        'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png',
                        'size' => 'sm',
                      ),
                      1 => 
                      array (
                        'type' => 'icon',
                        'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png',
                        'size' => 'sm',
                      ),
                      2 => 
                      array (
                        'type' => 'icon',
                        'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png',
                        'size' => 'sm',
                      ),
                      3 => 
                      array (
                        'type' => 'icon',
                        'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png',
                        'size' => 'sm',
                      ),
                      4 => 
                      array (
                        'type' => 'icon',
                        'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gray_star_28.png',
                        'size' => 'sm',
                      ),
                      5 => 
                      array (
                        'type' => 'text',
                        'text' => "$rating",
                        'flex' => 0,
                        'margin' => 'md',
                        'size' => 'sm',
                        'color' => '#999999',
                      ),
                    ),
                  ),
                ),
              ),
              'footer' => 
              array (
                'type' => 'box',
                'layout' => 'vertical',
                'flex' => 0,
                'spacing' => 'sm',
                'contents' => 
                array (
                  0 => 
                  array (
                    'type' => 'button',
                    'action' => 
                    array (
                      'type' => 'postback',
                      'label' => 'Detail Kelas',
                      'data' => 'detail#'.$key->course_id,
                    ),
                    'color' => '#FA4E4E',
                    'style' => 'primary',
                  ),
                ),
              ),
            );
            if($i==9){
            break;
            }else{
              $i++;
            }
            array_push($itemKelasArr,$itemKelas);
          }
        $content=array (
            'type' => 'carousel',
            'contents' => $itemKelasArr
        );
        $result=$messageBuilder->flex("Kelas Luarsekolah",$content);
        }else{
        $result=$messageBuilder->text("List kelas belum tersedia");
        }
        
        return $result;
    }
    function getRecommendation(){

    }
    function getChapter($course_id){
      $messageBuilder=new MessageBuilder();
      $this->load->model('Course_model');
      $getChapter=$this->Course_model->chapter($course_id);
      $itemsModul=[];
      if($getChapter->num_rows()==0){
        return $messageBuilder->text("Modul belajar belum tersedia");
      }else{
        foreach($getChapter->result() as $key){
          if($key->is_subchapter==1){//jika chapter memiliki subchapter
            $labelButton= "Lihat Submodul";
          }else{
            $labelButton= "Lihat Materi";
          }
          
          $itemModul=array (
            'type' => 'bubble',
            'body' => 
            array (
              'type' => 'box',
              'layout' => 'vertical',
              'contents' => 
              array (
                0 => 
                array (
                  'type' => 'text',
                  'text' => $key->chapter_title,
                  'size' => 'md',
                  'align' => 'start',
                  'weight' => 'bold',
                  'wrap' => true,
                ),
                1 => 
                array (
                  'type' => 'box',
                  'layout' => 'baseline',
                  'margin' => 'md',
                  'contents' => 
                  array (
                    0 => 
                    array (
                      'type' => 'text',
                      'text' => $key->subject,
                      'flex' => 0,
                      'margin' => 'md',
                      'size' => 'sm',
                      'align' => 'start',
                      'color' => '#999999',
                      'wrap' => true,
                    ),
                  ),
                ),
                2 => 
                array (
                  'type' => 'image',
                  'url' => 'https://cdnls.ams3.digitaloceanspaces.com/imagebot/course/'.urlencode($key->image),
                  'margin' => 'md',
                  'align' => 'center',
                  'gravity' => 'center',
                  'size' => 'full',
                  'aspectRatio' => '1.51:1',
                  'aspectMode' => 'cover',
                ),
              ),
            ),
            'footer' => 
            array (
              'type' => 'box',
              'layout' => 'vertical',
              'flex' => 0,
              'spacing' => 'sm',
              'contents' => array (
                0 => 
                array (
                  'type' => 'button',
                  'action' => 
                  array (
                    'type' => 'postback',
                    'label' => $labelButton,
                    'data' => 'subchapter#'.$key->chapter_id
                  ),
                  'color'=>'#00CC99',
                  'style' => 'primary',
                )
              ),
             
            ),
          );
          array_push($itemsModul,$itemModul);
        }
        $content=array (
          'type' => 'carousel',
          'contents' => $itemsModul
        );
        $result=$messageBuilder->flex("Modul Belajar",$content);
        return $result;
      }
    }
    function getSubchapter($chapter_id){
      $messageBuilder=new MessageBuilder();
      $this->load->model('Course_model');
      $getSubChapter=$this->Course_model->subchapter($chapter_id);
      $itemsModul=[];
      if($getSubChapter->num_rows()==0){
        return $messageBuilder->text("Materi belajar belum tersedia");
      }else{
        foreach($getSubChapter->result() as $key){
          $contentButton=[];
          if($key->video!='' && $key->video != NULL && $key->video !='-'){
            // https://liff.line.me/1654761938-zD5vxw3Z&video=
            if (stripos($key->video, 'youtube') !== false) {
              $action=array (
                'type' => 'uri',
                'label' => 'Video',
                'uri' => 'https://liff.line.me/1654761938-zD5vxw3Z?video='.$key->video
              );
            }else if(stripos($key->video, '.mp4') === false && ($key->video!='-')){
              $action=array (
                'type' => 'uri',
                'label' => 'Video',
                'uri' => 'https://liff.line.me/1654761938-zD5vxw3Z?video='.$key->video
              );
            }else if(stripos($key->video, '.mp4') !== false){ 
              $action=array (
                'type' => 'postback',
                'label' => 'Video',
                'data' => 'materi#video#'.$key->video
              );
            }
            $buttonVideo=array (
              'type' => 'button',
              'action' => 
              $action,
              'color'=>'#00CC99',
              'style' => 'primary',
            );
            array_push($contentButton,$buttonVideo);
          }
          if($key->audio!='' && $key->audio != NULL && $key->audio !='-'){
            $buttonAudio=array (
              'type' => 'button',
              'action' => 
              array (
                'type' => 'postback',
                'label' => 'Podcast',
                'data' => 'materi#audio#'.$key->audio
              ),
              'color'=>'#00CC99',
              'style' => 'primary',
            );
            array_push($contentButton,$buttonAudio);
          }
          if($key->ebook!='' && $key->ebook != NULL & $key->ebook !='-'){
            $buttonEbook=array (
              'type' => 'button',
              'action' => 
              array (
                'type' => 'uri',
                'label' => 'Ebook',
                'uri' => 'https://docs.google.com/viewer?url=https://cdnls.ams3.digitaloceanspaces.com/docs/course/'.urlencode($key->ebook),
              ),
              'color'=>'#00CC99',
              'style' => 'primary',
            );
            array_push($contentButton,$buttonEbook);
          }
          if($key->is_subchapter==1){//jika chapter memiliki subchapter
            $title= $key->title;
            $subtitle= $key->chapter_title;
          }else{
            $title= $key->chapter_title;
            $subtitle= $key->subject;
          }
          
          $itemModul=array (
            'type' => 'bubble',
            'body' => 
            array (
              'type' => 'box',
              'layout' => 'vertical',
              'contents' => 
              array (
                0 => 
                array (
                  'type' => 'text',
                  'text' => $title,
                  'size' => 'md',
                  'align' => 'start',
                  'weight' => 'bold',
                  'wrap' => true,
                ),
                1 => 
                array (
                  'type' => 'box',
                  'layout' => 'baseline',
                  'margin' => 'md',
                  'contents' => 
                  array (
                    0 => 
                    array (
                      'type' => 'text',
                      'text' => $subtitle,
                      'flex' => 0,
                      'margin' => 'md',
                      'size' => 'sm',
                      'align' => 'start',
                      'color' => '#999999',
                      'wrap' => true,
                    ),
                  ),
                )
              ),
            ),
            'footer' => 
            array (
              'type' => 'box',
              'layout' => 'vertical',
              'flex' => 0,
              'spacing' => 'sm',
              'contents' => $contentButton
             
            ),
          );
          array_push($itemsModul,$itemModul);
        }
        $content=array (
          'type' => 'carousel',
          'contents' => $itemsModul
        );
        $result=$messageBuilder->flex("Materi",$content);
        return $result;
      }
    }
    
    function getDetail($course_id){
      $messageBuilder=new MessageBuilder();
      $this->load->model('Course_model');
      $getDetail=$this->Course_model->getCoursebyId($course_id);
      if($getDetail->num_rows()>0){
        $getDetail=$getDetail->row();
        $content=array(
          'type' => 'bubble',
          'direction' => 'ltr',
          'body' => 
          array (
            'type' => 'box',
            'layout' => 'vertical',
            'contents' => 
            array (
              0 => 
              array (
                'type' => 'text',
                'text' => $getDetail->subject,
                'size' => 'md',
                'weight' => 'bold',
                'wrap' => true,
              ),
              1 => 
              array (
                'type' => 'separator',
                'margin' => 'md',
              ),
              2 => 
              array (
                'type' => 'text',
                'text' => substr(strip_tags($getDetail->description),0,300),
                'margin' => 'md',
                'wrap' => true,
              ),
            ),
          ),
          'footer' => 
          array (
            'type' => 'box',
            'layout' => 'vertical',
            'flex' => 0,
            'spacing' => 'sm',
            'contents' => 
            array (
              0 => 
              array (
                'type' => 'button',
                'action' => 
                array (
                  'type' => 'uri',
                  'label' => 'Lihat Selengkapnya',
                  'uri' => 'https://www.luarsekolah.com/kelas/online/'.$getDetail->slug,
                ),
                'color' => '#0035B9',
                'style' => 'link',
              ),
              1 => 
              array (
                'type' => 'button',
                'action' => 
                array (
                  'type' => 'postback',
                  'label' => 'Beli Kelas',
                  'data' => 'checkout#'.$getDetail->course_id,
                ),
                'color' => '#FA4E4E',
                'style' => 'primary',
              ),
              2 => 
              array (
                'type' => 'button',
                'action' => 
                array (
                  'type' => 'postback',
                  'label' => 'Redeem Voucher',
                  'data' => 'redeem voucher',
                ),
                'color' => '#FA4E4E',
                'style' => 'primary',
              ),
            ),
          ),
        );
        $result=$messageBuilder->flex("Detail Kelas",$content);
      }else{
        $result=$messageBuilder->text("Detail kelas tidak ditemukan");
      }
      return $result;
        
    }
    
    function player(){
      $video=$this->input->get('liff_state');
      $video=urldecode($video);
      $video=explode("=",$video);
      $video=$video[1];
      $data['video']=$video;
      $this->load->view('video',$data);

    }
}