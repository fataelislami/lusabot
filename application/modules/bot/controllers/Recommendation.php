<?php

/**
 * PHP item based filtering
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * @package   PHP item based filtering
 */

class Recommendation extends MY_Controller{
    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this
            ->load
            ->model(array(
            'Recommendation_model','Course_model','Transaction_model'
        ));

    }

    function sendFlex($user_id,$gender,$job){
        $mb=new MessageBuilder();
        // $this->db->limit(9);
        $getData=$this->Recommendation_model->getRatingCourseUser();
        $datasets=[];
        $result_recommendation=FALSE;
        //mengambil dataset
        foreach($getData->result() as $key){
            $subject=explode(",",$key->subject);
            $ratings=explode(",",$key->ratings);
            $ratingData=array_combine($subject, $ratings);
            $datasets["$key->users_id"]=$ratingData;
        }
        // membuat object dari library recommendation
        $re = new Recommendation();
        $checkTransaction=$this->Transaction_model->historyData($user_id);
        if($checkTransaction->num_rows()>0){
            $result=$re->getRecommendations($datasets, $user_id);
            $course_subject=[];
            $i=1;
            foreach($result as $key=>$value){
                array_push($course_subject,$key);
                if($i==9){
                break;
                }
                $i++;
            }
            // var_dump($course_subject);die;
            if(sizeof($course_subject)!=0){
              $this->db->where_in("course.subject",$course_subject);
              $getCourse=$this->Course_model->getAllData();
              var_dump($getCourse->result());
              $result_recommendation=TRUE;
            }else{
              $getRelatedCourse=$this->Course_model->getRelatedCourse($gender,$job);
              $course_subject=[];
              if($getRelatedCourse->num_rows()>0){
                  foreach($getRelatedCourse->result() as $key){
                      array_push($course_subject,$key->subject);
                  }
                  // var_dump($course_subject);
                  $this->db->where_in("course.subject",$course_subject);
                  $getCourse=$this->Course_model->getAllData();
                  var_dump($getCourse->result());
                  $result_recommendation=TRUE;
              }else{
                  // Tidak ada course yang dapat direkomendasikan
                  $result_recommendation=FALSE;
              }
            }

        }else{//pengecekan course berdasarkan data latar belakang pengguna
            $getRelatedCourse=$this->Course_model->getRelatedCourse($gender,$job);
            $course_subject=[];
            if($getRelatedCourse->num_rows()>0){
                foreach($getRelatedCourse->result() as $key){
                    array_push($course_subject,$key->subject);
                }
                // var_dump($course_subject);
                $this->db->where_in("course.subject",$course_subject);
                $getCourse=$this->Course_model->getAllData();
                var_dump($getCourse->result());
                $result_recommendation=TRUE;
            }else{
                // Tidak ada course yang dapat direkomendasikan
                $result_recommendation=FALSE;
            }
        }
        if($result_recommendation){
            $itemKelasArr=[];
          $i=1;
          foreach($getCourse->result() as $key){
            $rating=round(($key->rata_rata!=NULL?$key->rata_rata:0),1);
            $itemKelas=array(
              'type' => 'bubble',
              'direction' => 'ltr',
              'hero' => 
              array (
                'type' => 'image',
                'url' => 'https://cdnls.ams3.digitaloceanspaces.com/imagebot/course/'.urlencode($key->image),
                'size' => 'full',
                'aspectRatio' => '20:13',
                'aspectMode' => 'cover',
              ),
              'body' => 
              array (
                'type' => 'box',
                'layout' => 'vertical',
                'contents' => 
                array (
                  0 => 
                  array (
                    'type' => 'text',
                    'text' => $key->subject,
                    'size' => 'lg',
                    'weight' => 'bold',
                    'wrap'=>true
                  ),
                  1 => 
                  array (
                    'type' => 'text',
                    'text' => 'Rp.'.number_format($key->price,0,',','.'),
                    'margin' => 'lg',
                    'size' => 'md',
                    'align' => 'start',
                    'gravity' => 'center',
                    'weight' => 'bold',
                  ),
                  2 => 
                  array (
                    'type' => 'box',
                    'layout' => 'baseline',
                    'margin' => 'md',
                    'contents' => 
                    array (
                      0 => 
                      array (
                        'type' => 'icon',
                        'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png',
                        'size' => 'sm',
                      ),
                      1 => 
                      array (
                        'type' => 'icon',
                        'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png',
                        'size' => 'sm',
                      ),
                      2 => 
                      array (
                        'type' => 'icon',
                        'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png',
                        'size' => 'sm',
                      ),
                      3 => 
                      array (
                        'type' => 'icon',
                        'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gold_star_28.png',
                        'size' => 'sm',
                      ),
                      4 => 
                      array (
                        'type' => 'icon',
                        'url' => 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/review_gray_star_28.png',
                        'size' => 'sm',
                      ),
                      5 => 
                      array (
                        'type' => 'text',
                        'text' => "$rating",
                        'flex' => 0,
                        'margin' => 'md',
                        'size' => 'sm',
                        'color' => '#999999',
                      ),
                    ),
                  ),
                ),
              ),
              'footer' => 
              array (
                'type' => 'box',
                'layout' => 'vertical',
                'flex' => 0,
                'spacing' => 'sm',
                'contents' => 
                array (
                  0 => 
                  array (
                    'type' => 'button',
                    'action' => 
                    array (
                      'type' => 'postback',
                      'label' => 'Detail Kelas',
                      'data' => 'detail#'.$key->course_id,
                    ),
                    'color' => '#FA4E4E',
                    'style' => 'primary',
                  ),
                ),
              ),
            );
            if($i==9){
            break;
            }else{
              $i++;
            }
            array_push($itemKelasArr,$itemKelas);
          }
        $content=array (
            'type' => 'carousel',
            'contents' => $itemKelasArr
        );
        $result=$mb->flex("Kelas Luarsekolah",$content);
            return $result;
        }else{
        $result=$mb->text("Belum ada kelas yang cocok untuk kamu");
            return $result;
        }
    }
    
    
    public function similarityDistance($preferences, $person1, $person2)
    {
        $similar = array();
        $sum = 0;
        
        foreach($preferences[$person1] as $key=>$value)
        {
            if(array_key_exists($key, $preferences[$person2]))
                $similar[$key] = 1;
        }
        
        if(count($similar) == 0)
            return 0;
        
        foreach($preferences[$person1] as $key=>$value)
        {
            if(array_key_exists($key, $preferences[$person2]))
                $sum = $sum + pow($value - $preferences[$person2][$key], 2);
        }
        
        return  1/(1 + sqrt($sum));     
    }
    
    
    public function matchItems($preferences, $person)
    {
        $score = array();
        
            foreach($preferences as $otherPerson=>$values)
            {
                if($otherPerson !== $person)
                {
                    $sim = $this->similarityDistance($preferences, $person, $otherPerson);
                    
                    if($sim > 0)
                        $score[$otherPerson] = $sim;
                }
            }
        
        array_multisort($score, SORT_DESC);
        return $score;
    
    }
    
    
    public function transformPreferences($preferences)
    {
        $result = array();
        
        foreach($preferences as $otherPerson => $values)
        {
            foreach($values as $key => $value)
            {
                $result[$key][$otherPerson] = $value;
            }
        }
        
        return $result;
    }
    
    
    public function getRecommendations($preferences, $person)
    {
        $total = array();
        $simSums = array();
        $ranks = array();
        $sim = 0;
        
        foreach($preferences as $otherPerson=>$values)
        {
            if($otherPerson != $person)
            {
                $sim = $this->similarityDistance($preferences, $person, $otherPerson);
            }
            
            if($sim > 0)
            {
                foreach($preferences[$otherPerson] as $key=>$value)
                {
                    if(!array_key_exists($key, $preferences[$person]))
                    {
                        if(!array_key_exists($key, $total)) {
                            $total[$key] = 0;
                        }
                        $total[$key] += $preferences[$otherPerson][$key] * $sim;
                        
                        if(!array_key_exists($key, $simSums)) {
                            $simSums[$key] = 0;
                        }
                        $simSums[$key] += $sim;
                    }
                }
                
            }
        }

        foreach($total as $key=>$value)
        {
            $ranks[$key] = $value / $simSums[$key];
        }
        
    array_multisort($ranks, SORT_DESC);    
    return $ranks;
        
    }
   
}

?>