<?php

class Login extends MY_Controller{
    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this
            ->load
            ->model(array(
            'Login_model'
        ));

    }

    function isLoggedIn($user_id){
        $where=array('line_identity'=>$user_id);
        $getData=$this->Login_model->checkData($where);
        if($getData->num_rows()>0){
            return true;
        }else{
            return false;
        }
    }
    function sendFlex($line_identity){
        $result=array (
            'type' => 'bubble',
            'header' => 
            array (
              'type' => 'box',
              'layout' => 'vertical',
              'flex' => 0,
              'contents' => 
              array (
                0 => 
                array (
                  'type' => 'button',
                  'action' => 
                  array (
                    'type' => 'uri',
                    'label' => 'MASUK',
                    'uri' => base_url().'bot/login?line_identity='.$line_identity,
                  ),
                  'color' => '#00CC99',
                  'margin' => 'lg',
                  'style' => 'primary',
                ),
                1 => 
                array (
                  'type' => 'box',
                  'layout' => 'horizontal',
                  'spacing' => 'none',
                  'margin' => 'md',
                  'contents' => 
                  array (
                    0 => 
                    array (
                      'type' => 'text',
                      'text' => 'Belum memiki akun?',
                      'margin' => 'none',
                      'size' => 'xs',
                      'align' => 'center',
                      'color' => '#252525',
                    ),
                  ),
                ),
                2 => 
                array (
                  'type' => 'button',
                  'action' => 
                  array (
                    'type' => 'uri',
                    'label' => 'DAFTAR',
                    'uri' => base_url().'bot/register?line_identity='.$line_identity,
                  ),
                  'margin' => 'xs',
                  'style' => 'link',
                ),
              ),
            ),
        );
        return $result;

    }
    function index(){
        $line_identity=$this->input->get('line_identity');
        $data['line_identity']=$line_identity;
        $this->load->view('login',$data);
    }
    function action(){
        require('Bot.php');
        $mb=new MessageBuilder();
        $bot=new Bot();
        $email=$this->input->post("email");
        $password=$this->input->post("password");
        $line_identity=$this->input->post("line_identity");
        $where=array('email'=>$email);
        $getData=$this->Login_model->checkData($where);
        if($getData->num_rows()>0){
            $getData=$getData->row();
            $hashed_pass=$getData->password;
            if (!password_verify($password, $hashed_pass))
            {
                $bot->push(array($mb->text("Password Salah")),$line_identity);
                redirect("https://line.me/R/oaMessage/@293mfwar/?%20");
            }
            else
            {
                $dataUpdate=array('line_identity'=>$line_identity);
                $updateUsers=$this->Login_model->update($where,$dataUpdate);
                $bot->push(array($mb->text("Login Berhasil")),$line_identity);
                redirect("https://line.me/R/oaMessage/@293mfwar/?%20");
            }
        }else{
          $bot->push(array($mb->text("Email tidak ditemukan")),$line_identity);
          redirect("https://line.me/R/oaMessage/@293mfwar/?%20");
        }

    }

    function logout($line_identity){
        $where=array('line_identity'=>$line_identity);
        $dataUpdate=array('line_identity'=>NULL);
        $updateUsers=$this->Login_model->update($where,$dataUpdate);
        if($updateUsers){
            return true;
        }else{
            return false;
        }
    }
}