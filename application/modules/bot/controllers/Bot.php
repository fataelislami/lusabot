<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once ('line_class.php');
require_once ('MessageBuilder.php');
require_once ('Winnowing.php');
require_once ('Login.php');
class Bot extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this
        ->load
        ->model(array(
        'Bot_model'
    ));
    }

    public function webhook()
    {

        //Konfigurasi Chatbot
//sesuaikan
        $channelAccessToken = 'VrJE5SszG6E09XcFM4I+Ilz1u7E3yyWwjDkJSJwPEmTKcx/1XJGibUxx8Ctni8hjsGX6r8SMoGJLGDuKvb0DV6cRZz5nTniNJ7Vy8ExE81uTba3MzoIbNTM3dSauLjQC3q11ekaONKbU1NIIfi6bJwdB04t89/1O/w1cDnyilFU=';
        $channelSecret = 'cfb46d3be2dae5f97186e5642a865f48'; 
        //Konfigurasi Chatbot END
        $client = new LINEBotTiny($channelAccessToken, $channelSecret);
        $messageBuilder = new MessageBuilder();
        $login = new Login();

        $userId = $client->parseEvents() [0]['source']['userId'];
        $replyToken = $client->parseEvents() [0]['replyToken'];
        $timestamp = $client->parseEvents() [0]['timestamp'];
        $profil = $client->profil($userId);
        $nama = $profil->displayName;
        $pecahnama = explode(" ", $profil->displayName);
        $namapanggilan = $pecahnama[0];
        $event = $client->parseEvents() [0];
        $isLoggedIn=$login->isLoggedIn($userId);
        $displayPicture = $profil->pictureUrl;
        $userData=$this->Bot_model->getUsers($userId)->row();
        if ($event['type'] == 'follow') //Yang bot lakukan pertama kali saat di add oleh user
        {
            $pre = [];
            $msg1 = $messageBuilder->text("Halo sobat Lusa selamat datang di Luarsekolah");
            $msg2 = $messageBuilder->text("untuk menggunakan bot Lusa cek keyword dibawah ini ya\n1.Rekomendasi Kelas\n2.List Kelas\n3.Kelas Saya\n4.Riwayat Transaksi\n5.Kelola Profile\n6.Logout\n7.Daftar");

            array_push($pre, $msg1, $msg2);
            $output = $this->reply($replyToken, $pre);
        }
        if ($event['type'] == 'message') //membaca event message
        {
            $message = $client->parseEvents() [0]['message'];
            $inputMessage = strtoupper($message['text']);
            $keyword=$this->checkKeyword($inputMessage);//mengecek inputan untuk pencarian keyword
            if ($userData->flag=='voucher'){
                require_once ('Voucher.php');
                $oVoucher = new Voucher();
                if($inputMessage=='RESET'){
                    $pre = [];
                    if($isLoggedIn){
                        $msg1 = $oVoucher->resetFlag($userId);
                        array_push($pre,$msg1);
                    }else{
                        $msg1 = $messageBuilder->text("Silakan login terlebih dahulu");
                        $msg2 = $messageBuilder->flex("Autentikasi",$login->sendFlex($userId));
                        array_push($pre,$msg1,$msg2);
                    }
    
                    $output = $this->reply($replyToken, $pre);
                }else 
                {
                    $pre=[];
                    $msg1 = $oVoucher->getVoucher($message['text']);
                    array_push($pre,$msg1);
                    $output = $this->reply($replyToken, $pre);
                }
            }
            else if ($keyword == 'DAFTAR')
            {
                $pre=[];
                if($isLoggedIn){
                    $msg1 = $messageBuilder->text("Sudah Login");
                    array_push($pre,$msg1);
                }else{
                    require('Register.php');
                    $register=new Register();
                    $msg1 = $messageBuilder->text("Silakan untuk daftar terlebih dahulu");
                    $msg2 = $messageBuilder->flex("Daftar",$register->sendFlex($userId));
                    array_push($pre,$msg1,$msg2);
                }

                $output = $this->reply($replyToken, $pre);
            }
            else if ($keyword == 'LOGIN')
            {
                $pre=[];
                if($isLoggedIn){
                    $msg1 = $messageBuilder->text("Sudah Login");
                    array_push($pre,$msg1);
                }else{
                    $msg1 = $messageBuilder->text("Silakan login terlebih dahulu");
                    $msg2 = $messageBuilder->flex("Autentikasi",$login->sendFlex($userId));
                    array_push($pre,$msg1,$msg2);
                }

                $output = $this->reply($replyToken, $pre);
            }
            else if ($keyword=='rekomendasi kelas')
            {
                require_once("Recommendation.php");
                $oRecommendation=new Recommendation();
                $pre = [];
                if($isLoggedIn){
                    $msg1 = $messageBuilder->text("Berikut rekomendasi kelas yang cocok untuk kak ".$namapanggilan);
                    $msg2 = $oRecommendation->sendFlex($userData->users_id,$userData->gender,$userData->job);
                    array_push($pre, $msg1,$msg2);
                }else{
                    $msg1 = $messageBuilder->text("Silakan untuk login terlebih dahulu");
                    $msg2 = $messageBuilder->flex("Daftar",$login->sendFlex($userId));
                    array_push($pre,$msg1,$msg2);
                }
                
                $output = $this->reply($replyToken, $pre);
            }
            else if ($keyword=='kelola profile')
            {
                $pre=[];
                if($isLoggedIn){
                    require("Users.php");
                    $oUsers=new Users();
                    $msg1 = $oUsers->sendProfile($userId,$displayPicture);
                    array_push($pre,$msg1);
                }else{
                    $msg1 = $messageBuilder->text("Silakan untuk login terlebih dahulu");
                    $msg2 = $messageBuilder->flex("Daftar",$login->sendFlex($userId));
                    array_push($pre,$msg1,$msg2);
                }

                $output = $this->reply($replyToken, $pre);
            }
            else if ($keyword=='kelas saya')
            {
                require_once ('Users.php');
                $oUsers = new Users();
                $pre = [];
                if($isLoggedIn){
                    $msg1 = $oUsers->getCourse($userData->users_id);
                    array_push($pre, $msg1);
                }else{
                    $msg1 = $messageBuilder->text("Silakan untuk login terlebih dahulu");
                    $msg2 = $messageBuilder->flex("Daftar",$login->sendFlex($userId));
                    array_push($pre,$msg1,$msg2);
                }
                $output = $this->reply($replyToken, $pre);
            }
            else if ($keyword=='list kelas')
            {
                require_once ('Course.php');
                $oCourse = new Course();
                $pre = [];
                $msg1 = $oCourse->getListCourse();
                array_push($pre, $msg1);
                $output = $this->reply($replyToken, $pre);
            }
            else if ($keyword == 'P')
            {
                if($isLoggedIn){
                    $pre = array(
                        $messageBuilder->text("Sudah Login\n".$userId)
                    );
                }else{
                    $pre = array(
                        $messageBuilder->text("Belum Login")
                    );
                }

                $output = $this->reply($replyToken, $pre);
            }
            else if ($keyword=='riwayat transaksi')
            {
                $pre = [];
                if($isLoggedIn){
                    require('Transaction.php');
                    $transaction=new Transaction();
                    $msg1 = $transaction->history($userData->users_id);
                    // $msg1 = $messageBuilder->text("Name ".$paramsData['course_id']);
                    array_push($pre,$msg1);
                }else{
                    $msg1 = $messageBuilder->text("Silakan login terlebih dahulu");
                    $msg2 = $messageBuilder->flex("Autentikasi",$login->sendFlex($userId));
                    array_push($pre,$msg1,$msg2);
                }
                $output = $this->reply($replyToken, $pre);
            }
            // Logout
            else if ($keyword == 'LOGOUT')
            {
                $logout=$login->logout($userId);
                if($logout){
                    $pre = array(
                        $messageBuilder->text("Anda telah keluar")
                    );
                }else{
                    $pre = array(
                        $messageBuilder->text("Anda belum login")
                    );
                }

                $output = $this->reply($replyToken, $pre);
            }else if ($keyword == 'lihat keyword')
            {
                $pre = array(
                    $messageBuilder->text("Hai! untuk menggunakan bot Lusa cek keyword dibawah ini ya\n1.Rekomendasi Kelas\n2.List Kelas\n3.Kelas Saya\n4.Riwayat Transaksi\n5.Kelola Profile\n6.Logout\n7.Daftar")
                );
                $output = $this->reply($replyToken, $pre);
            }
            else{
                $pre = array(
                    $messageBuilder->text("Maaf kak Lusa gangerti dengan permintaan tersebut atau kaka bisa ketik lihat keyword ya untuk bantuan.")
                );
                $output = $this->reply($replyToken, $pre);
            }
        }
        if ($event['type'] == 'postback') //membacara event postback
        {
            $postback = $client->parseEvents() [0]['postback'];
            // Postback melihat detail kelas
            if (substr($postback['data'], 0, 6) == "detail")
            {
                require_once ('Course.php');
                $course_id=explode("#",$postback['data']);
                $course_id=$course_id[1];
                $oCourse = new Course();
                $pre = [];
                $msg1 = $oCourse->getDetail($course_id);
                array_push($pre,$msg1);
                $output = $this->reply($replyToken, $pre);
            }
            // Postback melihat chapter kelas
            if (substr($postback['data'], 0, 7) == "belajar")
            {
                require_once ('Course.php');
                $course_id=explode("#",$postback['data']);
                $course_id=$course_id[1];
                $oCourse = new Course();
                $pre = [];
                if($isLoggedIn){
                    $msg1 = $oCourse->getChapter($course_id);
                    array_push($pre,$msg1);
                }else{
                    $msg1 = $messageBuilder->text("Silakan login terlebih dahulu");
                    $msg2 = $messageBuilder->flex("Autentikasi",$login->sendFlex($userId));
                    array_push($pre,$msg1,$msg2);
                }
                $output = $this->reply($replyToken, $pre);
            }
            // Postback melihat subchapter kelas
            if (substr($postback['data'], 0, 10) == "subchapter")
            {
                require_once ('Course.php');
                $chapter_id=explode("#",$postback['data']);
                $chapter_id=$chapter_id[1];
                $oCourse = new Course();
                $pre = [];
                if($isLoggedIn){
                    $msg1 = $oCourse->getSubchapter($chapter_id);
                    array_push($pre,$msg1);
                }else{
                    $msg1 = $messageBuilder->text("Silakan login terlebih dahulu");
                    $msg2 = $messageBuilder->flex("Autentikasi",$login->sendFlex($userId));
                    array_push($pre,$msg1,$msg2);
                }

                $output = $this->reply($replyToken, $pre);
            }
            // Postback melihat video materi
            if (substr($postback['data'], 0, 6) == "materi")
            {
                $pre = [];
                $postdata=explode("#",$postback['data']);
                $filetype=$postdata[1];
                $filename=$postdata[2];
                if($filetype=='video'){
                    $url="https://cdnls.ams3.digitaloceanspaces.com/course/".$filename;
                    $msg1 = $messageBuilder->video($url,"https://cdnls.ams3.digitaloceanspaces.com/imagebot/course/startup.jpg");
                }else{
                    $url="https://cdnls.ams3.digitaloceanspaces.com/auds/course/".$filename;
                    $msg1 = $messageBuilder->audio($url);
                }
                
                array_push($pre,$msg1);
                $output = $this->reply($replyToken, $pre);
            }
            // Postback membeli kelas
            if (substr($postback['data'], 0, 8) == "checkout")
            {
                $course_id=explode("#",$postback['data']);
                $course_id=$course_id[1];
                $paramsData=array(
                    'course_id'=>$course_id,
                    'user_id'=>$userData->users_id,
                    'name'=>$userData->fullname,
                    'email'=>$userData->email
                );
                $pre = [];
                if($isLoggedIn){
                    require('Transaction.php');
                    $transaction=new Transaction();
                    $msg1 = $transaction->create($paramsData);
                    // $msg1 = $messageBuilder->text("Name ".$paramsData['course_id']);
                    array_push($pre,$msg1);
                }else{
                    $msg1 = $messageBuilder->text("Silakan login terlebih dahulu");
                    $msg2 = $messageBuilder->flex("Autentikasi",$login->sendFlex($userId));
                    array_push($pre,$msg1,$msg2);
                }
                $output = $this->reply($replyToken, $pre);
            }
            // Postback redeem kelas
            if ($postback['data']== "redeem voucher")
            {
                require_once ('Voucher.php');
                $oVoucher = new Voucher();
                $pre = [];
                if($isLoggedIn){
                    $msg1 = $oVoucher->updateFlag($userId);
                    array_push($pre,$msg1);
                }else{
                    $msg1 = $messageBuilder->text("Silakan login terlebih dahulu");
                    $msg2 = $messageBuilder->flex("Autentikasi",$login->sendFlex($userId));
                    array_push($pre,$msg1,$msg2);
                }

                $output = $this->reply($replyToken, $pre);
            }else
            if (substr($postback['data'], 0, 6) == "redeem")
            {
                require_once ('Voucher.php');
                $oVoucher = new Voucher();
                $postbackData=explode("#",$postback['data']);
                $code=$postbackData[2];
                $status=$postbackData[1];//cek apakah ya atau tidak
                $pre = [];
                if($status=='ya'){
                    $msg1 = $oVoucher->redeem($code,$userData->users_id);
                    $oVoucher->resetFlag($userId);//reset sesi
                    array_push($pre,$msg1);
                }else{
                    $msg1 = $oVoucher->resetFlag($userId);
                    array_push($pre,$msg1);
                }
                $output = $this->reply($replyToken, $pre);
            }
            // Postback redeem voucher kelas
            
        }        
        $client->replyMessage($output);

    }

    public function reply($replyToken, $messageArray)
    { //Fungsi utama
        $reply = array(
            'replyToken' => $replyToken,
            'messages' => $messageArray
        );
        return $reply;
    }

    function push($arrayMessage,$bot_id){//ganti kondisi ini
    $channelAccessToken = 'VrJE5SszG6E09XcFM4I+Ilz1u7E3yyWwjDkJSJwPEmTKcx/1XJGibUxx8Ctni8hjsGX6r8SMoGJLGDuKvb0DV6cRZz5nTniNJ7Vy8ExE81uTba3MzoIbNTM3dSauLjQC3q11ekaONKbU1NIIfi6bJwdB04t89/1O/w1cDnyilFU=';
    $channelSecret = 'cfb46d3be2dae5f97186e5642a865f48';
    $client = new LINEBotTiny($channelAccessToken, $channelSecret);
    $push = array(
            'to' => $bot_id,
            'messages' => $arrayMessage
            );
    
    $client->pushMessage($push);
    }

    public function checkKeyword($masukan)
    {
        $keywords=["kelas saya","rekomendasi kelas","list kelas","kelola profile","riwayat transaksi","lihat keyword"];
        $keyword="";
        $max=0;
        foreach($keywords as $key){
            
            $w = new Winnowing($masukan, $key);
            $w->SetPrimeNumber(2);
            $w->SetNGramValue(4);
            $w->SetNWindowValue(3);
            $w->process();
            $temp=$w->GetJaccardCoefficient();
            if($max<$temp){
                $keyword=$key;
                $max=$temp;
            }
            // echo $key." : Similarity".$temp."<br>";
        }
        if($max>10){
            return $keyword;
        }else{
            return $masukan;
        }
    }
}

